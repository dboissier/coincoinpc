import re
import time
from collections import defaultdict
from operator import itemgetter


def extract_studio_and_country(filiale):
    match = re.search('(.*) \((.*)\)', filiale)
    if match is None:
        return None

    return match.group(1), match.group(2)


def extract_year_of_issue(e):
    date_in_ddMMYYYY = e['issueDate']
    parsed_date = time.strptime(date_in_ddMMYYYY, '%d/%m/%Y')
    return parsed_date[0]


def group_by_year(data):
    grouped = group_by(lambda e: extract_year_of_issue(e), data)
    return sorted([(k, sorted(v, key=itemgetter('title'), reverse=True)) for k, v in grouped.items()])


def group_by_year2(data):
    grouped = group_by(lambda e: int(e['year']), data)
    return sorted([(k, sorted(v, key=itemgetter('title'))) for k, v in grouped.items()])


def group_by_alphabet(data, key=None):
    grouped = group_by_alphabetic(lambda elem: elem[key][0].upper(), data)
    return sorted([(k, sorted(v, key=itemgetter(key))) for k, v in grouped.items()])


def group_by_score(data):
    grouped = group_by(lambda e: int(e['score']), data)
    return sorted([(k, sorted(v, key=itemgetter('title'))) for k, v in grouped.items()])


def group_by_genre(data):
    grouped = group_by(lambda e: e['primaryGenre'], data)
    return sorted([(k, sorted(v, key=itemgetter('title'))) for k, v in grouped.items()])


def group_by_category(data):
    grouped = group_by(lambda e: e['category'], data)
    return sorted([(k, sorted(v, key=itemgetter('title'))) for k, v in grouped.items()])


def group_by_reviewer(data):
    grouped = group_by(lambda e: e['reviewer']['name'], data)
    return sorted([(k, sorted(v, key=itemgetter('title'))) for k, v in grouped.items()])


def group_by(func, data):
    result = defaultdict(list)
    for elt in data:
        result[func(elt)].append(elt)
    return result


def group_by_alphabetic(func, data):
    result = defaultdict(list)
    for elt in data:
        key = func(elt)
        if re.match('[A-Z]', key):
            result[key].append(elt)
        else:
            result['0-9'].append(elt)
    return result


def convert_list_of_pair_to_list_of_dict(data):
    result = []
    for value_one, value_two in data:
        result.append({"%s" % value_one: value_two})
    return result
