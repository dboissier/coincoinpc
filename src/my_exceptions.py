class MissingMongoConfiguration(Exception):
    def __str__(self):
        return "Mongo configuration is unavailable"


class InvalidData(Exception):
    def __init__(self, errors):
        self.errors = errors
