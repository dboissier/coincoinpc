# -*- coding: utf-8 -*-
import json

from flask import Blueprint, current_app, render_template, request, redirect, url_for

from data_access import BoardReviewDao, BoardMagazineDao
from security_access import hash_data

bp = Blueprint('plato', __name__, template_folder='templates')


@bp.route('')
@bp.route('/')
def to_redirect_to_index():
    return redirect(url_for('plato.plato_index'))


@bp.route('/index.html')
def plato_index():
    return render_template('v3/plato/index.html',
                           statistics={'boardGameEntryCount': board_review_dao(get_mongo_client_reader()).count()},
                           api_token=hash_data(request.host_url))


@bp.route('/search.html')
def plato_search():
    filter_data_str = request.args.get('filter', '')
    if filter_data_str:
        filter_data = json.loads(filter_data_str)
        board_reviews = board_review_dao(get_mongo_client_reader()).get_all_review_by(filter_data)

        return render_template('v3/plato/search_result.html', reviews=board_reviews, api_token=hash_data(request.host_url))

    search_value = request.args.get('searchInput', '')
    if search_value:
        board_reviews = board_review_dao(get_mongo_client_reader()).get_all_review_by_title(search_value)

        return render_template('v3/plato/search_result.html', reviews=board_reviews, api_token=hash_data(request.host_url))

    return redirect(url_for('plato.plato_index'))


@bp.route('/review/<review_id>.html')
def plato_review(review_id: str):
    board_review = board_review_dao(get_mongo_client_reader()).get_review_by_id(review_id)

    return render_template('v3/plato/board_review.html', review=board_review)


@bp.route('/magazine/<issue_id>.html')
def plato_magazine(issue_id: str):
    magazine = board_magazine_dao(get_mongo_client_reader()).get_magazine_by_issue_number(issue_id)

    return render_template('v3/plato/board_magazine.html', magazine=magazine)


@bp.route('/magazines.html')
def magazines_html():
    magazines = board_magazine_dao(get_mongo_client_reader()).get_all_magazines()
    return render_template('v3/plato/magazines.html', magazines=magazines)


@bp.route('/indexes/title.html')
def index_title():
    reviews = board_review_dao(get_mongo_client_reader()).get_all_review_titles()
    return render_template('v3/plato/index_title.html', reviews=reviews)


@bp.route('/about.html')
def about():
    return render_template('v3/plato/about.html')


def board_review_dao(mongo_client):
    return BoardReviewDao(mongo_client)


def board_magazine_dao(mongo_client):
    return BoardMagazineDao(mongo_client)


def get_mongo_client_reader():
    return current_app.config['MONGO_READER']
