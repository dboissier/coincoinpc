import os

from pymongo import MongoClient

from my_exceptions import MissingMongoConfiguration


class MyMongoClientReader:
    def __init__(self, env="test"):
        if env == "test":
            # localhost
            self.mongo_client = MongoClient(port=27017)
            self.database = self.mongo_client['canardpc']
        else:
            # Production
            if not all([key in os.environ for key in
                        ["MONGO_HOST", "MONGO_PORT", "MONGO_DATABASE", "MONGO_USERNAME", "MONGO_PASSWORD"]]):
                raise MissingMongoConfiguration()
            else:
                mongo_host = os.environ["MONGO_HOST"]
                mongo_database = os.environ["MONGO_DATABASE"]
                mongo_username = os.environ["MONGO_USERNAME"]
                mongo_password = os.environ["MONGO_PASSWORD"]

                mongo_uri = "mongodb+srv://%s:%s@%s/%s" % (
                    mongo_username, mongo_password, mongo_host, mongo_database)
                self.mongo_client = MongoClient(mongo_uri)
                self.database = self.mongo_client[mongo_database]

    def close(self):
        self.mongo_client.close()


class MyMongoClientWriter:
    def __init__(self, env="test"):
        if env == "test":
            print(f'running in {env} environment')
            # localhost
            self.mongo_client = MongoClient(port=27017)
            self.database = self.mongo_client['canardpc']
        else:
            # Production
            if not all([key in os.environ for key in
                        ["MONGO_HOST", "MONGO_PORT", "MONGO_DATABASE", "MONGO_USERNAME_RW", "MONGO_PASSWORD_RW"]]):
                raise MissingMongoConfiguration()
            else:
                mongo_host = os.environ["MONGO_HOST"]
                mongo_port = int(os.environ["MONGO_PORT"])
                mongo_database = os.environ["MONGO_DATABASE"]
                mongo_username = os.environ["MONGO_USERNAME_RW"]
                mongo_password = os.environ["MONGO_PASSWORD_RW"]

                mongo_uri = "mongodb+srv://%s:%s@%s/%s" % (
                    mongo_username, mongo_password, mongo_host, mongo_database)
                self.mongo_client = MongoClient(mongo_uri)
                self.database = self.mongo_client[mongo_database]

    def close(self):
        self.mongo_client.close()
