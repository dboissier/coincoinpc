import os
from dataclasses import dataclass
from pprint import pprint

import requests


@dataclass
class UserProfile:
    nickname: str
    profile_url: str
    avatar_url: str
    steam_id: str
    realname: str


class SteamClient:

    def __init__(self, api_key):
        self.api_key = api_key

    def fetch_game_info(self, steam_appid):
        response = requests.get(f'https://store.steampowered.com/api/appdetails/?appids={steam_appid}&l=fr',
                                headers={
                                    'Accept-Language': 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3'
                                })

        if response.status_code == 200:
            game_info = response.json()
            if game_info[str(steam_appid)]['success']:
                return game_info[str(steam_appid)]['data']
        return None

    def find_steam_userid(self, steam_nickname):
        response = requests.get(
            f'https://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key={self.api_key}&vanityurl={steam_nickname}')
        if response.status_code == 200:
            user_info_data = response.json()['response']
            if user_info_data['success'] == 42:
                return None
            return user_info_data['steamid']
        else:
            return None

    def fetch_user_info_summary(self, steam_id):
        response = requests.get(
            f'https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002?key={self.api_key}&steamids={steam_id}')
        if response.status_code == 200:
            response_body = response.json()['response']['players']
            if response_body:
                first_entry = response_body[0]
                return UserProfile(
                    nickname=first_entry['personaname'],
                    avatar_url=first_entry['avatarfull'],
                    profile_url=first_entry['avatarfull'],
                    steam_id=steam_id,
                    realname=first_entry['realname'] if 'realname' in first_entry else None
                )

        return None

    def fetch_user_games(self, steam_id):
        response = requests.get(
            f'https://api.steampowered.com/IPlayerService/GetOwnedGames/v0001?key={self.api_key}&steamid={steam_id}&include_appinfo=true')
        if response.status_code == 200:
            response_body = response.json()['response']
            if 'games' in response_body:
                return response_body['games']

        return []

    def fetch_user_whishlist(self, steam_id):
        response = requests.get(
            f'https://store.steampowered.com/wishlist/profiles/{steam_id}/wishlistdata',
            headers={
                'Accept-Language': 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3'
            })
        if response.status_code == 200:
            if 'application/json' in response.headers['Content-Type']:
                return response.json()
        return []


GAMES_TO_SAVE = [
    433910,
    593150,
    687440,
    716670,
    378360,
]

if __name__ == '__main__':
    steam_client = SteamClient(api_key=os.environ['STEAM_API_KEY'])
    game_info = steam_client.fetch_game_info(steam_appid=236430)
    if game_info:
        pprint(game_info)
