from string import Template

import pymongo
from bson.objectid import ObjectId

import data_utils
from my_exceptions import InvalidData

TEMPLATE_REGEX = Template('.*$value.*')


def generate_regex_filter(value):
    return {'$regex': TEMPLATE_REGEX.substitute(value=value), '$options': 'i'}


class ReviewDao(object):
    def __init__(self, mongo_client):
        self.database = mongo_client.database

    def get_all_review_titles(self, group_by_alphabet=True):
        reviews = list(self.database['review'].find({}, {'title': 1, 'score': 1, 'otherScore': 1}).sort('title',
                                                                                                        pymongo.ASCENDING))

        if group_by_alphabet:
            return data_utils.group_by_alphabet(reviews, key='title')
        else:
            return reviews

    def get_all_review_by_title(self, title):
        reviews = []
        for review in self.database['review'] \
                .find({'title': generate_regex_filter(title)}) \
                .sort([('issue.issueNumber', pymongo.DESCENDING), ('title', pymongo.ASCENDING)]):
            review['_id'] = str(review['_id'])
            reviews.append(review)
        return reviews

    def get_all_review_by(self, search_filter):

        if len(search_filter) == 0:
            return []

        if 'title' in search_filter:
            search_filter['title'] = generate_regex_filter(search_filter['title'])

        return [review for review in self.database['review'].find(search_filter)]

    def get_all_game_titles(self):
        return [review['title'] for review in
                self.database['review'].find({}, {'title': 1, '_id': None}).sort('title', pymongo.ASCENDING)]

    def get_reviews_without_critic(self):
        return [review for review in
                self.database['review'].find({'critic': ''}, {'title': 1, 'issue': 1}).limit(30).sort(
                    'issue.issueNumber', pymongo.DESCENDING)]

    def get_reviews_by_reviewer(self, reviewer_name):
        reviews = list(self.database['review'].find({'reviewer.name': reviewer_name}))
        return data_utils.group_by_year2(reviews)

    def get_review_by_title(self, title):
        return self.database['review'].find_one({'title': title})

    def get_review_by_steam_appid(self, steam_appid):
        review = self.database['review'].find_one({'steamAppId': steam_appid})

        return review

    def get_review_by_oid(self, oid):
        return self.database['review'].find_one({'_id': ObjectId(oid)})

    def create_review(self, review_to_save):
        self.enrich_review(review_to_save)

        return self.database['review'].insert_one(review_to_save).inserted_id

    def update_review(self, id, review_to_save):
        self.enrich_review(review_to_save)

        return self.database['review'].replace_one({'_id': ObjectId(id)}, review_to_save)

    def enrich_review(self, review_to_save):
        reviewer = self.database['reviewer'].find_one({'name': review_to_save['reviewer']},
                                                      {'_id': False})
        magazine = self.database['magazine'].find_one({'issueNumber': review_to_save['issueNumber']},
                                                      {'_id': False, 'coverUrl': False, 'title': False,
                                                       'subTitle': False})
        errors = []
        if not reviewer:
            errors.append("Testeur '%s' inconnu" % review_to_save['reviewer'])
        if not magazine:
            errors.append("Magazine '%s' inconnu" % review_to_save['issueNumber'])
        if errors:
            raise InvalidData(errors)

        review_to_save['reviewer'] = reviewer
        review_to_save['issue'] = magazine

        if 'steamAppId' in review_to_save and review_to_save['steamAppId']:
            review_to_save['steamAppId'] = int(review_to_save['steamAppId'])

        score = review_to_save['score']
        if type(score) is int or score.isdigit():
            review_to_save['score'] = int(score)
        else:
            review_to_save['otherScore'] = review_to_save['score']
            review_to_save['score'] = -1

    def get_all_reviews(self):
        return [review for review in self.database['review'].find()]

    def get_all_scores(self, group_by_score=True):
        scores = [score for score in
                  self.database['review'].find({}, {'_id': 1, 'title': 1, 'score': 1, 'otherScore': 1})]

        if group_by_score:
            return data_utils.group_by_score(scores)
        else:
            return scores

    def get_all_reviewers(self):
        genres = list(
            self.database['review'].find({}, {'reviewer.name': 1, 'title': 1, '_id': 1, 'score': 1, 'otherScore': 1}))
        return data_utils.group_by_reviewer(genres)

    def get_all_genres(self):
        genres = list(
            self.database['review'].find({}, {'primaryGenre': 1, 'title': 1, '_id': 1, 'score': 1, 'otherScore': 1}))
        return data_utils.group_by_genre(genres)

    def get_all_genres_name(self):
        return list(self.database['review'].find({}, {'primaryGenre': 1}).distinct('primaryGenre'))

    def get_reviews_by_genre(self, genre_name):
        reviews = list(self.database['review'].find({'primaryGenre': genre_name}))
        return data_utils.group_by_year2(reviews)

    def get_reviews_by_studio(self, studio_name):
        reviews = list(self.database['review'].find({'studioName': studio_name}))
        return data_utils.group_by_year2(reviews)

    def get_reviews_by_publisher(self, publisher_name):
        reviews = list(self.database['review'].find({'publisher': publisher_name}))
        return data_utils.group_by_year2(reviews)

    def get_review_by_year(self, year):
        reviews = [review for review in self.database['review'].find({'year': year})]

        if len(reviews) > 0:
            return {'year': year, 'review': reviews}

    def update_reviewer(self, reviewer):
        self.database['review'].update_many({'reviewer.name': reviewer['name']},
                                            {'$set': {
                                                'reviewer': reviewer
                                            }})

    def update_magazine_issue(self, magazine_data):
        magazine_issue = {
            'issueNumber': magazine_data['issueNumber'],
            'coverTnUrl': magazine_data['coverTnUrl'],
        }
        self.database['review'].update_many({'issue.issueNumber': magazine_data['issueNumber']},
                                            {'$set': {
                                                'issue': magazine_issue
                                            }})

    def get_count(self):
        return self.database['review'].count_documents({})

    def get_count_with_steam(self):
        return self.database['review'].count_documents({'steamAppId': {'$exists': True}})


class BoardReviewDao(object):
    def __init__(self, mongo_client):
        self.database = mongo_client.database

    def get_all_review_by(self, search_filter):
        mongo_filter = dict()
        if 'gameStyles' in search_filter and search_filter['gameStyles']:
            if 'extension' in search_filter['gameStyles']:
                mongo_filter['extension'] = True
                search_filter['gameStyles'].remove('extension')

            if search_filter['gameStyles']:
                mongo_filter['category'] = {
                    '$in': search_filter['gameStyles']
                }

        if 'duration' in search_filter:
            duration_filter = dict()
            if search_filter['duration'][0] > 30:
                duration_filter['$gte'] = search_filter['duration'][0]
            if search_filter['duration'][1] < 120:
                duration_filter['$lte'] = search_filter['duration'][1]

            if duration_filter:
                mongo_filter['duration.value'] = duration_filter

        if 'complexity' in search_filter:
            mongo_filter['complexity.value'] = {
                '$gte': search_filter['complexity'][0],
                '$lte': search_filter['complexity'][1],
            }

        if 'nbPlayers' in search_filter:
            nb_players_filter = dict()
            nb_players_filter['$gte'] = search_filter['nbPlayers'][0]
            if search_filter['nbPlayers'][1] < 5:
                nb_players_filter['$lte'] = search_filter['nbPlayers'][1]

            mongo_filter['nbPlayers.range'] = {
                '$elemMatch': nb_players_filter
            }

        if 'price' in search_filter:
            price_filter = dict()
            if search_filter['price'][0] > 20:
                price_filter['$gte'] = search_filter['price'][0]
            if search_filter['price'][1] < 60:
                price_filter['$lte'] = search_filter['price'][1]

            if price_filter:
                mongo_filter['price.value'] = price_filter

        print(mongo_filter)

        return [review for review in self.database['board_review'].find(mongo_filter)]

    def get_review_by_id(self, review_id):
        return self.database['board_review'].find_one({'_id': ObjectId(review_id)})

    def get_all_review_by_title(self, title):
        reviews = []
        for review in self.database['board_review'] \
                .find({'title': generate_regex_filter(title)}) \
                .sort([('title', pymongo.ASCENDING)]):
            review['_id'] = str(review['_id'])
            reviews.append(review)
        return reviews

    def get_all_reviews_by_mag_issue_number(self, issue_number):
        reviews = []
        for review in self.database['board_review'] \
                .find({'issue.issueNumber': issue_number}) \
                .sort([('title', pymongo.ASCENDING)]):
            review['_id'] = str(review['_id'])
            reviews.append(review)
        return reviews

    def get_all_review_titles(self):
        reviews = list(self.database['board_review'].find({}, {'title': 1}).sort('title', pymongo.ASCENDING))

        return data_utils.group_by_alphabet(reviews, key='title')

    def count(self):
        return self.database['board_review'].count_documents({})


class MagazineDao(object):
    def __init__(self, mongo_client):
        self.database = mongo_client.database

    def get_all_magazine_years(self):
        return sorted(list({data_utils.extract_year_of_issue(magazine) for magazine in
                            self.database['magazine']
                           .find({}, {'_id': False, 'issueDate': 1})}))

    def get_all_magazines(self, group_by_year=True):
        magazines = [magazine for magazine in
                     self.database['magazine'].find({'$query': {}, '$orderby': {'issueNumber': 1}}, {'_id': False})]

        if group_by_year:
            return data_utils.group_by_year(magazines)
        else:
            return magazines

    def get_all_magazines_by_year(self, year):
        return [magazine for magazine in
                self.database['magazine'].find(
                    {'$query': {'issueDate': {'$regex': ".*%s" % year}}, '$orderby': {'issueNumber': 1}},
                    {'_id': False}).sort('issueNumber', pymongo.DESCENDING)]

    def get_magazines(self):
        return [magazine for magazine in
                self.database['magazine'].find({'$query': {}, '$orderby': {'issueNumber': 1}})
                .sort('issueNumber', pymongo.DESCENDING)]

    def get_magazine_by_issue_number(self, issue_number):
        magazine = self.database['magazine'].find_one({'issueNumber': issue_number}, {'_id': False})
        if magazine:
            pc_reviews = []
            console_reviews = []
            for review in self.database['review'].find({'issue.issueNumber': issue_number}):
                if review['plateform'] == 'PC':
                    pc_reviews.append(review)
                else:
                    console_reviews.append(review)
            magazine['reviews'] = {
                'PC': pc_reviews,
                'console': console_reviews
            }

        return magazine

    def create_magazine(self, magazine_to_save):
        return self.database['magazine'].insert_one(magazine_to_save).inserted_id

    def update_magazine(self, id, magazine_to_save):
        return self.database['magazine'].replace_one({'_id': ObjectId(id)}, magazine_to_save)

    def get_empty_magazines(self):
        review_issues = list(
            self.database['review'].find({}, {'issue.issueNumber': 1, '_id': False}).distinct('issue.issueNumber'))
        magazines = list(self.database['magazine'].find({}, {'issueNumber': 1, '_id': False}).distinct('issueNumber'))

        empty_magazines = set(magazines) - set(review_issues)
        return sorted(list(empty_magazines), reverse=True)[:30]

    def get_count(self):
        return self.database['magazine'].count_documents({})


class BoardMagazineDao(object):
    def __init__(self, mongo_client):
        self.database = mongo_client.database

    def get_magazine_by_issue_number(self, issue_number):
        magazine = self.database['board_magazine'].find_one({'issueNumber': issue_number}, {'_id': False})
        if magazine:
            reviews = [review for review in self.database['board_review'].find({'issue.issueNumber': issue_number})]
            magazine['reviews'] = data_utils.group_by_category(reviews)

        return magazine

    def get_all_magazines(self):
        return [magazine for magazine in
                self.database['board_magazine'].find({'$query': {}}).sort('issueNumber', pymongo.DESCENDING)]


class ReviewerDao(object):
    def __init__(self, mongo_client):
        self.database = mongo_client.database

    def get_all_reviewers(self):
        return [reviewer for reviewer in self.database['reviewer'].find()]

    def get_reviewer_by_name(self, reviewer_name):
        return self.database['reviewer'].find_one({'name': reviewer_name}, {'_id': False})

    def get_stats_for_a_reviewer(self, reviewer_name, dict_format=False):
        max_score_occurrence, total_by_score = self.get_reviewer_number_by_score(reviewer_name)
        max_genre_occurrence, total_by_genre = self.get_reviewer_number_by_genre(reviewer_name)

        if dict_format:
            total_by_score = data_utils.convert_list_of_pair_to_list_of_dict(total_by_score)
            total_by_genre = data_utils.convert_list_of_pair_to_list_of_dict(total_by_genre)

        return {'byScore': {'max': max_score_occurrence, 'totalByScore': total_by_score},
                'byGenre': {'max': max_genre_occurrence, 'totalByGenre': total_by_genre}}

    def get_reviewer_number_by_score(self, reviewer_name):
        aggregation_result = self.database['review'].aggregate(
            [{'$match': {'reviewer.name': reviewer_name}}, {'$project': {'score': 1}},
             {'$group': {'_id': '$score', 'total': {'$sum': 1}}}])

        score_aggregation_result = []
        occurrence_max = 0
        for score_aggregation in aggregation_result:
            total_for_a_score = score_aggregation['total']
            occurrence_max = max(occurrence_max, total_for_a_score)
            score_aggregation_result.append((score_aggregation['_id'], total_for_a_score))
        return occurrence_max, sorted(score_aggregation_result)

    def get_reviewer_number_by_genre(self, reviewer_name):
        aggregation_result = self.database['review'].aggregate(
            [{'$match': {'reviewer.name': reviewer_name}}, {'$project': {'primaryGenre': 1}},
             {'$group': {'_id': '$primaryGenre', 'total': {'$sum': 1}}}])

        genre_aggregation_result = []
        occurrence_max = 0
        for genre_aggregation in aggregation_result:
            total_for_a_genre = genre_aggregation['total']
            occurrence_max = max(occurrence_max, total_for_a_genre)
            genre_aggregation_result.append((genre_aggregation['_id'], total_for_a_genre))
        return occurrence_max, sorted(genre_aggregation_result, key=lambda total_by_score: total_by_score[1],
                                      reverse=True)

    def create_reviewer(self, reviewer_to_save):
        if reviewer_to_save['reviewerTnUrl'] == '':
            reviewer_to_save['reviewerTnUrl'] = 'https://assets.coincoinpc.fr/reviewer-avatars/default.jpg'

        return self.database['reviewer'].insert_one(reviewer_to_save).inserted_id

    def update_reviewer(self, id, reviewer_to_save):
        if reviewer_to_save['reviewerTnUrl'] == '':
            reviewer_to_save['reviewerTnUrl'] = 'https://assets.coincoinpc.fr/reviewer-avatars/default.jpg'

        return self.database['reviewer'].replace_one({'_id': ObjectId(id)}, reviewer_to_save)


class SteamAppInfoDao(object):
    def __init__(self, mongo_client):
        self.database = mongo_client.database
        self.collection_name = 'steam_game_info'

    def get_game_info_by_id(self, steam_app_id):
        return self.database[self.collection_name].find_one({'steam_appid': steam_app_id})

    def create_game_info(self, game_info):
        return self.database[self.collection_name].insert_one(game_info)

    def update_game_info(self, id, game_info):
        return self.database[self.collection_name].replace_one({'_id': id}, game_info)

    def get_count(self):
        return self.database[self.collection_name].count_documents({})


class EventDao(object):
    def __init__(self, mongo_client):
        self.database = mongo_client.database

    def save_event(self, event):
        self.database['event'].insert_one(event)

    def get_last_events(self, nb=30):
        return self.database['event'].find().limit(nb).sort('timestamp', pymongo.DESCENDING)
