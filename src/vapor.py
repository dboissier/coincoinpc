import os
import re

from flask import Blueprint, request

from data_access import ReviewDao, SteamAppInfoDao
from my_mongo_client import MyMongoClientReader, MyMongoClientWriter
from steam_client import SteamClient

env = "test"
if 'ENV' in os.environ:
    env = os.environ['ENV']

mongo_client_reader = MyMongoClientReader(env)
mongo_client_writer = MyMongoClientWriter(env)

bp = Blueprint('vapor', __name__, template_folder='templates')

REMASTERED_GAMES_MAP = {
    '409720': 8850,
    '409710': 7670,
    '12240': 12110,
    '12250': 12120,
    '17340': 17330,
    '21120': 21090,
    '28050': 238010,
    '41010': 41014,
}

GAMES_TO_IGNORE = [
    201280,
    440430,
    440431,
    440432,
    440433,
    440434,
    440435,
    440436,
    440437,
    440438,
    440439,
    1368430,
    21970,
    58510,
    12750,
    204080
]

@bp.route('/steam-games', methods=['GET'])
def find_steam_games_by_nickname():
    steam_nickname = request.args.get('steamNickname', '')
    if not steam_nickname:
        return f'No nickname provided', 400

    client = steam_client()
    steam_userid = client.find_steam_userid(steam_nickname=steam_nickname)
    if steam_userid is None:
        return f"Pas d'infos Steam trouvées avec le pseudo '{steam_nickname}'", 404
    return find_games_with_steam_id(client, steam_userid)


@bp.route('/steam-games/<steam_id>', methods=['GET'])
def find_steam_games_by_steamid(steam_id):
    client = steam_client()
    return find_games_with_steam_id(client, steam_id)


def find_games_with_steam_id(client, steam_id):
    user_info_summary = client.fetch_user_info_summary(steam_id=steam_id)
    if user_info_summary is None:
        return f"Pas d'infos Steam trouvées avec l'id Steam '{steam_id}'", 404

    user_info_summary = {
        'nickname': user_info_summary.nickname,
        'profile_url': user_info_summary.profile_url,
        'avatar_url': user_info_summary.avatar_url,
        'steam_id': user_info_summary.steam_id,
        'realname': user_info_summary.realname
    }
    steam_games = client.fetch_user_games(steam_id=steam_id)
    steam_wished_games = client.fetch_user_whishlist(steam_id=steam_id)
    review_dao = create_review_dao(mongo_client_reader)
    steam_app_dao = create_steam_app_dao(mongo_client_writer)
    user_games = []
    year_regex = re.compile(r'\d{4}')
    for steam_game in steam_games:
        steam_appid = get_steam_appid(steam_game['appid'])
        game_review = review_dao.get_review_by_steam_appid(steam_appid=steam_appid)

        if game_review:
            user_games.append({
                "reviewUrl": f"/review/{game_review['_id']}.html",
                "coverTnUrl": game_review['coverTnUrl'],
                "title": steam_game['name'],
                "subTitle": game_review['subTitle'],
                "year": int(game_review['year']),
                "reviewer": {
                    "name": game_review['reviewer']['name'],
                    "reviewerTnUrl": game_review['reviewer']['reviewerTnUrl']
                },
                "issueNumber": game_review['issue']['issueNumber'],
                "critic": game_review['critic'],
                "otherScore": game_review['otherScore'] if 'otherScore' in game_review else -1,
                "score": game_review['score'],
                "steamInfo": {
                    "appid": steam_game['appid'],
                    "playtime_forever": steam_game['playtime_forever'],
                    "rtime_last_played": steam_game['rtime_last_played'],
                    "wished": False
                },
                "showCritic": False
            })
        else:
            steam_game_info = steam_app_dao.get_game_info_by_id(steam_appid)
            if not steam_game_info:
                if steam_appid in GAMES_TO_IGNORE:
                    continue
                print(f"Need to fetch game info of {steam_game['name']} with id={steam_appid} from steam")
                steam_game_info = client.fetch_game_info(steam_appid=steam_appid)
                if steam_game_info is None:
                    print(f"No steam game info of {steam_game['name']} with id={steam_appid}")
                    continue
                if steam_game_info['steam_appid'] == steam_appid:
                    steam_app_dao.create_game_info(steam_game_info)

            user_games.append({
                "reviewUrl": None,
                "coverTnUrl": steam_game_info['header_image'],
                "title": steam_game['name'],
                "subTitle": None,
                "year": extract_year_if_exists(steam_game_info, year_regex),
                "reviewer": None,
                "issueNumber": None,
                "critic": None,
                "otherScore": "Non testé",
                "score": -1,
                "steamInfo": {
                    "appid": steam_game['appid'],
                    "playtime_forever": steam_game['playtime_forever'],
                    "rtime_last_played": steam_game['rtime_last_played'],
                    "wished": False
                },
                "showCritic": False
            })

    if len(steam_wished_games) > 0:
        for key, wished_game_info in steam_wished_games.items():
            steam_appid = int(key)
            game_review = review_dao.get_review_by_steam_appid(steam_appid=steam_appid)
            if game_review:
                user_games.append({
                    "reviewUrl": f"/review/{game_review['_id']}.html",
                    "coverTnUrl": game_review['coverTnUrl'],
                    "title": game_review['title'],
                    "subTitle": game_review['subTitle'],
                    "year": int(game_review['year']),
                    "reviewer": {
                        "name": game_review['reviewer']['name'],
                        "reviewerTnUrl": game_review['reviewer']['reviewerTnUrl']
                    },
                    "issueNumber": game_review['issue']['issueNumber'],
                    "critic": game_review['critic'],
                    "otherScore": game_review['otherScore'] if 'otherScore' in game_review else -1,
                    "score": game_review['score'],
                    "steamInfo": {
                        "appid": steam_appid,
                        "playtime_forever": None,
                        "rtime_last_played": None,
                        "wished": True,
                        "discountInfo": build_discount_data(wished_game_info)
                    },
                    "showCritic": False
                })
            else:
                steam_game_info = steam_app_dao.get_game_info_by_id(steam_appid)
                if not steam_game_info:
                    print(f"Need to fetch game info of {wished_game_info['name']} with id={steam_appid} from steam")
                    steam_game_info = client.fetch_game_info(steam_appid=steam_appid)
                    if steam_game_info is None:
                        print(f"No steam game info of {wished_game_info['name']} with id={steam_appid}")
                        continue
                    if steam_game_info['steam_appid'] == steam_appid:
                        steam_app_dao.create_game_info(steam_game_info)

                user_games.append({
                    "reviewUrl": None,
                    "coverTnUrl": steam_game_info['header_image'],
                    "title": wished_game_info['name'],
                    "subTitle": None,
                    "year": extract_year_if_exists(steam_game_info, year_regex),
                    "reviewer": None,
                    "issueNumber": None,
                    "critic": None,
                    "otherScore": "Non testé",
                    "score": -1,
                    "steamInfo": {
                        "appid": steam_appid,
                        "playtime_forever": None,
                        "rtime_last_played": None,
                        "wished": True,
                        "discountInfo": build_discount_data(wished_game_info)
                    },
                    "showCritic": False
                })
    return {
        'user_infos': user_info_summary,
        'user_games': sorted(user_games, key=lambda game: game['title'])
    }


def build_discount_data(wished_game_info):
    if len(wished_game_info["subs"]) > 0:
        return {
            "discountPercent": wished_game_info["subs"][0]['discount_pct'] if wished_game_info["subs"][0]['discount_pct'] is not None else 0,
            "discountPrice": wished_game_info["subs"][0]['price']
        }
    return None


def extract_year_if_exists(steam_game_info, year_regex):
    if steam_game_info['release_date']:
        results = year_regex.findall(steam_game_info['release_date']['date'])
        if len(results) > 0:
            return int(results[0])
    return 0


def steam_client():
    return SteamClient(api_key=os.environ['STEAM_API_KEY'])


def create_review_dao(mongo_client):
    return ReviewDao(mongo_client)


def create_steam_app_dao(mongo_client):
    return SteamAppInfoDao(mongo_client)


def get_steam_appid(steam_appid):
    return REMASTERED_GAMES_MAP[str(steam_appid)] if str(steam_appid) in REMASTERED_GAMES_MAP else steam_appid