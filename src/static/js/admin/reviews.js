const {createApp} = Vue;

const countries = [
  {title: 'Afghanistan'},
  {title: 'Afrique du Sud'},
  {title: 'Albanie'},
  {title: 'Algérie'},
  {title: 'Allemagne'},
  {title: 'Andorre'},
  {title: 'Angleterre'},
  {title: 'Angola'},
  {title: 'Antigua-et-Barbuda'},
  {title: 'Arabie saoudite'},
  {title: 'Argentine'},
  {title: 'Arménie'},
  {title: 'Australie'},
  {title: 'Autriche'},
  {title: 'Azerbaïdjan'},
  {title: 'Bahamas'},
  {title: 'Bahreïn'},
  {title: 'Bangladesh'},
  {title: 'Barbade'},
  {title: 'Belau'},
  {title: 'Belgique'},
  {title: 'Belize'},
  {title: 'Bénin'},
  {title: 'Bhoutan'},
  {title: 'Biélorussie'},
  {title: 'Birmanie'},
  {title: 'Bolivie'},
  {title: 'Bosnie-Herzégovine'},
  {title: 'Botswana'},
  {title: 'Brésil'},
  {title: 'Brunei'},
  {title: 'Bulgarie'},
  {title: 'Burkina'},
  {title: 'Burundi'},
  {title: 'Cambodge'},
  {title: 'Cameroun'},
  {title: 'Canada'},
  {title: 'Cap-Vert'},
  {title: 'Chili'},
  {title: 'Chine'},
  {title: 'Chypre'},
  {title: 'Colombie'},
  {title: 'Comores'},
  {title: 'Congo'},
  {title: 'Congo'},
  {title: 'Cook'},
  {title: 'Corée du Nord'},
  {title: 'Corée du Sud'},
  {title: 'Costa Rica'},
  {title: 'Côte d\'Ivoire'},
  {title: 'Croatie'},
  {title: 'Cuba'},
  {title: 'Danemark'},
  {title: 'Djibouti'},
  {title: 'Dominique'},
  {title: 'Égypte'},
  {title: 'Émirats arabes unis'},
  {title: 'Équateur'},
  {title: 'Érythrée'},
  {title: 'Espagne'},
  {title: 'Estonie'},
  {title: 'États-Unis'},
  {title: 'Éthiopie'},
  {title: 'Fidji'},
  {title: 'Finlande'},
  {title: 'France'},
  {title: 'Gabon'},
  {title: 'Gambie'},
  {title: 'Géorgie'},
  {title: 'Ghana'},
  {title: 'Grèce'},
  {title: 'Grande-Bretagne'},
  {title: 'Grenade'},
  {title: 'Guatemala'},
  {title: 'Guinée'},
  {title: 'Guinée-Bissao'},
  {title: 'Guinée équatoriale'},
  {title: 'Guyana'},
  {title: 'Haïti'},
  {title: 'Honduras'},
  {title: 'Hongrie'},
  {title: 'Inde'},
  {title: 'Indonésie'},
  {title: 'Iran'},
  {title: 'Iraq'},
  {title: 'Irlande'},
  {title: 'Islande'},
  {title: 'Israël'},
  {title: 'Italie'},
  {title: 'Jamaïque'},
  {title: 'Japon'},
  {title: 'Jordanie'},
  {title: 'Kazakhstan'},
  {title: 'Kenya'},
  {title: 'Kirghizistan'},
  {title: 'Kiribati'},
  {title: 'Koweït'},
  {title: 'Laos'},
  {title: 'Lesotho'},
  {title: 'Lettonie'},
  {title: 'Liban'},
  {title: 'Liberia'},
  {title: 'Libye'},
  {title: 'Liechtenstein'},
  {title: 'Lituanie'},
  {title: 'Luxembourg'},
  {title: 'Macédoine'},
  {title: 'Madagascar'},
  {title: 'Malaisie'},
  {title: 'Malawi'},
  {title: 'Maldives'},
  {title: 'Mali'},
  {title: 'Malte'},
  {title: 'Maroc'},
  {title: 'Marshall'},
  {title: 'Maurice'},
  {title: 'Mauritanie'},
  {title: 'Mexique'},
  {title: 'Micronésie'},
  {title: 'Moldavie'},
  {title: 'Monaco'},
  {title: 'Mongolie'},
  {title: 'Mozambique'},
  {title: 'Namibie'},
  {title: 'Nauru'},
  {title: 'Népal'},
  {title: 'Nicaragua'},
  {title: 'Niger'},
  {title: 'Nigeria'},
  {title: 'Niue'},
  {title: 'Norvège'},
  {title: 'Nouvelle-Zélande'},
  {title: 'Oman'},
  {title: 'Ouganda'},
  {title: 'Ouzbékistan'},
  {title: 'Pakistan'},
  {title: 'Panama'},
  {title: 'Papouasie - Nouvelle Guinée'},
  {title: 'Paraguay'},
  {title: 'Pays-Bas'},
  {title: 'Pérou'},
  {title: 'Philippines'},
  {title: 'Pologne'},
  {title: 'Portugal'},
  {title: 'Qatar'},
  {title: 'République centrafricaine'},
  {title: 'République dominicaine'},
  {title: 'République tchèque'},
  {title: 'Roumanie'},
  {title: 'Royaume-Uni'},
  {title: 'Russie'},
  {title: 'Rwanda'},
  {title: 'Saint-Christophe-et-Niévès'},
  {title: 'Sainte-Lucie'},
  {title: 'Saint-Marin'},
  {title: 'Saint-Vincent-et-les Grenadines'},
  {title: 'Salomon'},
  {title: 'Salvador'},
  {title: 'Samoa occidentales'},
  {title: 'Sao Tomé-et-Principe'},
  {title: 'Sénégal'},
  {title: 'Seychelles'},
  {title: 'Sierra Leone'},
  {title: 'Singapour'},
  {title: 'Slovaquie'},
  {title: 'Slovénie'},
  {title: 'Somalie'},
  {title: 'Soudan'},
  {title: 'Sri Lanka'},
  {title: 'Suède'},
  {title: 'Suisse'},
  {title: 'Suriname'},
  {title: 'Swaziland'},
  {title: 'Syrie'},
  {title: 'Tadjikistan'},
  {title: 'Tanzanie'},
  {title: 'Tchad'},
  {title: 'Thaïlande'},
  {title: 'Togo'},
  {title: 'Tonga'},
  {title: 'Trinité-et-Tobago'},
  {title: 'Tunisie'},
  {title: 'Turkménistan'},
  {title: 'Turquie'},
  {title: 'Tuvalu'},
  {title: 'Ukraine'},
  {title: 'Uruguay'},
  {title: 'Vanuatu'},
  {title: 'Venezuela'},
  {title: 'Viêt Nam'},
  {title: 'Yémen'},
  {title: 'Yougoslavie'},
  {title: 'Zaïre'},
  {title: 'Zambie'},
  {title: 'Zimbabwe'}
];

const platforms = [
  {title: 'PC'},
  {title: 'PS3'},
  {title: 'Xbox 360'},
  {title: '3DS'},
  {title: 'DS'},
  {title: 'PS Vita'},
  {title: 'iOS'},
  {title: 'Android'},
  {title: 'PS4'},
  {title: 'Xbox One'},
  {title: 'Wii U'},
  {title: 'Wii'},
  {title: 'PS5'},
  {title: 'Switch'},
  {title: 'Xbox One X'}
];

createApp({
  compilerOptions: {
    delimiters: ['[[', ']]'],
  },

  data() {
    return {
      reviews: [],
      filteredReviews: [],
      reviewers: [],
      genres: [],
      countries: countries,
      platforms: platforms,
      reviewToSearch: '',
      issueToSearch: '',
      reviewForm: {
        id: null,
        title: '',
        subTitle: '',
        steamAppId: '',
        coverTnUrl: '',
        year: '',
        displayedGenre: '',
        primaryGenre: '',
        studioName: '',
        country: '',
        publisher: '',
        price: '',
        issueNumber: '',
        reviewer: '',
        plateform: '',
        critic: '',
        score: '',
        reviewUrl: '',
      }
    }
  },

  mounted() {
    $('.ui.search.country')
      .search({
        source: this.countries,
        onSelect: (result, _) => {
          this.reviewForm.country = result.title;
        }
      });
    $('.ui.search.plateform')
      .search({
        source: this.platforms,
        onSelect: (result, _) => {
          this.reviewForm.plateform = result.title;
        }
      });

    axios.get("/api/reviewers")
      .then((response) => {
        this.reviewers = response.data.map(reviewer => ({title: reviewer.name}));

        $('.ui.search.reviewer')
          .search({
            source: this.reviewers,
            onSelect: (result, _) => {
              this.reviewForm.reviewer = result.title;
            }
          });
      });

    axios.get('/api/genres')
      .then((response) => {
        this.genres = response.data.map(genre => ({title: genre}));

        $('.ui.search.primaryGenre')
          .search({
            source: this.genres,
            onSelect: (result, response) => {
              this.reviewForm.primaryGenre = result.title;
            }
          });
      });

    axios.get('/admin/reviews')
      .then((response) => {
        this.reviews = response.data
      });
  },

  methods: {
    searchReview() {
      if (this.reviewToSearch === "" && this.issueToSearch === "") {
        return [];
      }

      const reviewRegex = new RegExp(`.*${this.reviewToSearch}.*`, 'i');

      this.filteredReviews = this.reviews.filter((review) => {
        const titleMatch = reviewRegex.test(review.title);
        if (this.issueToSearch === "") {
          return titleMatch;
        }
        return review.issue.issueNumber === parseInt(this.issueToSearch) && titleMatch;
      });
    },

    editReview(review) {
      this.reviewForm.id = review._id["$oid"];
      this.reviewForm.title = review.title;
      this.reviewForm.subTitle = review.subTitle;
      this.reviewForm.steamAppId = review.steamAppId;
      this.reviewForm.coverTnUrl = review.coverTnUrl;
      this.reviewForm.year = review.year;
      this.reviewForm.displayedGenre = review.displayedGenre;
      this.reviewForm.primaryGenre = review.primaryGenre;
      this.reviewForm.studioName = review.studioName;
      this.reviewForm.country = review.country;
      this.reviewForm.publisher = review.publisher;
      this.reviewForm.price = review.price;
      this.reviewForm.issueNumber = review.issue.issueNumber;
      this.reviewForm.reviewer = review.reviewer.name;
      this.reviewForm.plateform = review.plateform;
      this.reviewForm.critic = review.critic;
      if (review.score === -1) {
        this.reviewForm.score = review.otherScore;
      } else {
        this.reviewForm.score = review.score;
      }
      this.reviewForm.reviewUrl = review['reviewUrl'] ? review.reviewUrl : '';
    },

    saveReview() {
      if (!this.reviewForm.id) {
        axios.post('/admin/review/save', this.prepareReviewData())
          .then((response) => {
            if (response.data.status === "SUCCESS") {
              this.reviewForm.id = response.data.id;
              this.displayMsg('success', `Jeu vid&eacute;o <strong>${this.reviewForm.title}</strong> cr&eacute;e`);
            } else {
              this.displayMsg('error', 'Une erreur est survenue : ' + response.data + '.')
            }
          })
          .catch((error) => {
            this.displayMsg('error', 'Une erreur est survenue : ' + error + '.')
          });
      } else {
        axios.put(`/admin/review/save/${this.reviewForm.id}`, this.prepareReviewData())
          .then((response) => {
            if (response.data.status === "SUCCESS") {
              this.reviewForm.id = response.data.id;
              this.displayMsg('success', `Jeu vid&eacute;o <strong>${this.reviewForm.title}</strong> mis &agrave; jour`);
            } else {
              this.displayMsg('error', `Une erreur est survenue : ${response.data}.`)
            }
          })
          .catch((error) => {
            this.displayMsg('error', `Une erreur est survenue : ${error}.`)
          })
      }
    },

    clearForm() {
      this.reviewForm.id = null;
      this.reviewForm.title = '';
      this.reviewForm.subTitle = '';
      this.reviewForm.steamAppId = '';
      this.reviewForm.coverTnUrl = '';
      this.reviewForm.year = '';
      this.reviewForm.displayedGenre = '';
      this.reviewForm.primaryGenre = '';
      this.reviewForm.studioName = '';
      this.reviewForm.country = '';
      this.reviewForm.publisher = '';
      this.reviewForm.price = '';
      this.reviewForm.issueNumber = '';
      this.reviewForm.reviewer = '';
      this.reviewForm.plateform = '';
      this.reviewForm.critic = '';
      this.reviewForm.score = '';
      this.reviewForm.reviewUrl = '';
    },

    prepareReviewData() {
      return {
        title: this.reviewForm.title,
        subTitle: this.reviewForm.subTitle,
        coverTnUrl: this.reviewForm.coverTnUrl,
        steamAppId: this.reviewForm.steamAppId,
        year: this.reviewForm.year,
        displayedGenre: this.reviewForm.displayedGenre,
        primaryGenre: this.reviewForm.primaryGenre,
        studioName: this.reviewForm.studioName,
        country: this.reviewForm.country,
        publisher: this.reviewForm.publisher,
        price: this.reviewForm.price,
        issueNumber: this.reviewForm.issueNumber,
        plateform: this.reviewForm.plateform,
        reviewer: this.reviewForm.reviewer,
        critic: this.reviewForm.critic,
        score: this.reviewForm.score,
        reviewUrl: this.reviewForm.reviewUrl,
      };
    },

    displayMsg(type, message) {
      $('body')
        .toast({
          class: type,
          message: message
        })
      ;
    }
  }
}).mount('#reviewForm');