const {createApp} = Vue

createApp({
  compilerOptions: {
    delimiters: ['[[', ']]'],
  },

  data() {
    return {
      magazines: [],
      issueToSearch: "",
      filteredMagazines: [],
      magazineIssueForm: {
        id: null,
        issueNumber: '',
        issueDate: '',
        title: '',
        subTitle: '',
        coverTnUrl: '',
        tableOfContentUrl: '',
      },
    }
  },

  mounted() {
    axios.get('/admin/magazines')
      .then((response) => {
        this.magazines = response.data;
      });
  },

  methods: {
    searchIssue() {
      if (this.issueToSearch === "") return [];

      this.filteredMagazines = this.magazines.filter((mag) => {
        return mag.issueNumber === parseInt(this.issueToSearch);
      });
    },

    editMagazineIssue(magazine) {
      this.magazineIssueForm.id = magazine._id["$oid"];
      this.magazineIssueForm.issueNumber = magazine.issueNumber;
      this.magazineIssueForm.issueDate = magazine.issueDate;
      this.magazineIssueForm.title = magazine.title;
      this.magazineIssueForm.subTitle = magazine.subTitle;
      this.magazineIssueForm.coverTnUrl = magazine.coverTnUrl;
      this.magazineIssueForm.tableOfContentUrl = magazine.tableOfContentUrl;
    },

    clearForm() {
      this.magazineIssueForm.id = null;
      this.magazineIssueForm.issueNumber = '';
      this.magazineIssueForm.issueDate = '';
      this.magazineIssueForm.title = '';
      this.magazineIssueForm.subTitle = '';
      this.magazineIssueForm.coverTnUrl = '';
      this.magazineIssueForm.tableOfContentUrl = '';
      this.feedback = '';
    },

    coverTnCdnUrl(event) {
      console.log(event);
      if (!event) {
        return '';
      }
      return `https://assets.coincoinpc.fr/mag-covers/${event.target.value}.jpg`;
    },

    saveMagazineIssue() {
      if (!this.magazineIssueForm.id) {
        axios.post('/admin/magazine/save', this.prepareMagazineIssueData())
          .then((response) => {
            if (response.data.status === "SUCCESS") {
              this.magazineIssueForm.id = response.data.id;
              this.displayMsg('success', `Magazine <strong>${this.magazineIssueForm.issueNumber}</strong> cr&eacute;e`);
            } else {
              this.displayMsg('error', `Une erreur est survenue : ${response.data}.`)
            }
          })
          .catch((error) => {
            this.displayMsg('error', `Une erreur est survenue : ${error}.`)
          });
      } else {
        axios.put(`/admin/magazine/save/${this.magazineIssueForm.id}`, this.prepareMagazineIssueData())
          .then((response) => {
            if (response.data.status === "SUCCESS") {
              this.magazineIssueForm.id = response.data.id;
              this.displayMsg('success', `Magazine <strong>${this.magazineIssueForm.issueNumber}</strong> mis &agrave; jour`);
            } else {
              this.displayMsg('error', `Une erreur est survenue : ${response.data}.`)
            }
          })
          .catch((error) => {
            this.displayMsg('error', `Une erreur est survenue : ${error}.`)
          })
      }
    },

    prepareMagazineIssueData() {
      return {
        title: this.magazineIssueForm.title,
        subTitle: this.magazineIssueForm.subTitle,
        issueNumber: this.magazineIssueForm.issueNumber,
        issueDate: this.magazineIssueForm.issueDate,
        coverTnUrl: this.magazineIssueForm.coverTnUrl,
        tableOfContentUrl: this.magazineIssueForm.tableOfContentUrl
      }
    },

    displayMsg(type, message) {
      $('body')
        .toast({
          class: type,
          message: message
        })
      ;
    }
  }
}).mount('#magazineForm');
