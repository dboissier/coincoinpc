const {createApp} = Vue;

createApp({
  compilerOptions: {
    delimiters: ['[[', ']]'],
  },

  data() {
    return {
      reviewers: [],
      filteredReviewers: [],
      reviewerNameToSearch: '',
      reviewerForm: {
        id: null,
        name: '',
        reviewerTnUrl: ''
      }
    }
  },

  mounted() {
    axios.get('/api/reviewers')
      .then((response) => {
        this.reviewers = response.data;
      });
  },

  methods: {
    searchReviewer() {
      if (this.reviewerNameToSearch === '') return [];
      const reviewRegex = new RegExp(`.*${this.reviewerNameToSearch}.*`, 'i');

      this.filteredReviewers = this.reviewers.filter((reviewer) => {
        return reviewRegex.test(reviewer.name);
      });
    },

    editReviewer(reviewer) {
      this.reviewerForm.id = reviewer._id['$oid'];
      this.reviewerForm.name = reviewer.name;
      this.reviewerForm.reviewerTnUrl = reviewer.reviewerTnUrl;
    },

    saveReviewer() {
      if (!this.reviewerForm.id) {
        axios.post('/admin/reviewer/save', this.prepareReviewerData())
          .then((response) => {
            if (response.data.status === "SUCCESS") {
              this.reviewerForm.id = response.data.id;
              this.displayMsg('success', `Testeur <strong>${this.reviewerForm.name}</strong> cr&eacute;&eacute;e`);
            } else {
              this.displayMsg('error', `Une erreur est survenue : ${response.data}.`)
            }
          })
          .catch((error) => {
            this.displayMsg('error', `Une erreur est survenue : ${error}.`)
          });
      } else {
        axios.put(`/admin/reviewer/save/${this.reviewerForm.id}`, this.prepareReviewerData())
          .then((response) => {
            if (response.data.status === "SUCCESS") {
              this.reviewerForm.id = response.data.id;
              this.displayMsg('success', `Testeur <strong>${this.reviewerForm.name}</strong> mis à jour`);
            } else {
              this.displayMsg('error', `Une erreur est survenue : ${response.data}.`)
            }
          })
          .catch((error) => {
            this.displayMsg('error', `Une erreur est survenue : ${error}.`)
          });
      }
    },

    prepareReviewerData() {
      return {
        name: this.reviewerForm.name,
        reviewerTnUrl: this.reviewerForm.reviewerTnUrl,
      };
    },

    clearForm() {
      this.reviewerForm.id = null;
      this.reviewerForm.name = '';
      this.reviewerForm.reviewerTnUrl = '';
    },

    displayMsg(type, message) {
      $('body')
        .toast({
          class: type,
          message: message
        });
    }
  }
}).mount('#reviewerForm');