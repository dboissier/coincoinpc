const {createApp} = Vue;

createApp({
  compilerOptions: {
    delimiters: ["[[", "]]"],
  },

  data() {
    return {
      steamNickname: "",
      steamId: "",
      error: null,
      step: "login",
      userInfos: null,
      foundGames: [],
      loadingForSteamNickname: false,
      loadingForSteamId: false,
      displayWishListOnly: false,
      searchGame: "",
      sortBy: "score",
      displayNeverPlayedGameOnly: false
    };
  },

  mounted() {
    this.enableUIComponents();

    const rawSavedData = localStorage.getItem("coincoinvapeur-games")
    if (rawSavedData) {
      const savedData = JSON.parse(rawSavedData);
      this.userInfos = savedData["user_infos"]
      this.foundGames = savedData["user_games"];
      this.step = "my_games";
    }
  },

  computed: {
    hasError() {
      return this.error !== null;
    },

    displayedGames() {
      if (this.sortBy === "score") {
        return _.orderBy(this.filteredGames, ["score", "title"], ["desc", "asc"]);
      } else if (this.sortBy === "spentTime") {
        return _.orderBy(this.filteredGames, [(game) => game.steamInfo.playtime_forever, "title"], ["desc", "asc"]);
      } else if (this.sortBy === "year") {
        return _.orderBy(this.filteredGames, ["year", "title"], ["desc", "asc"]);
      }

      return this.filteredGames;
    },

    wishedGames() {
      return _.filter(this.foundGames, (game) => {
        return game.steamInfo.wished;
      });
    },

    filteredGames() {
      const reviewRegex = new RegExp(`.*${this.searchGame}.*`, "i");

      return _.filter(this.foundGames, (game) => {
        return this.titleMatch(this.searchGame, reviewRegex, game) && this.neverPlayer(game) && this.fromWishList(game);
      });
    },

    meanScore() {
      if (this.filteredGames.length === 0) {
        return 0;
      }

      const gamesWithValidScore = _.filter(this.filteredGames, (game) => (game.score !== -1));

      if (gamesWithValidScore.length === 0) {
        return "NA";
      }

      const total = _.reduce(gamesWithValidScore, (sum, game) => (sum + game.score), 0)

      return Number(total / gamesWithValidScore.length).toFixed(2);
    },

    totalPlayedTime() {
      const total = _.reduce(this.filteredGames, (sum, game) => {
        return sum + game.steamInfo.playtime_forever;
      }, 0);

      return this.convertTime(total);
    },
  },

  methods: {
    findMyGamesBySteamNickname() {
      this.loadingForSteamNickname = true;
      this.message = null;
      axios.get(`/vapor/steam-games?steamNickname=${this.steamNickname}`)
        .then((response) => {
          const responseData = response.data;
          if (responseData["user_games"].length === 0) {
            this.error = `Pas de jeux trouvés avec le profil <strong>${this.steamNickname}</strong><br>Est-ce que votre profil est bien public ?`
          }
          this.processResponse(responseData);
        })
        .catch((error) => {
          if (error.response && error.response.status === 404) {
            this.error = error.response.data;
          }
        })
        .finally(() => {
          this.loadingForSteamNickname = false;
        });
    },

    findMyGamesBySteamId() {
      this.loadingForSteamId = true;
      this.message = null;
      axios.get(`/vapor/steam-games/${this.steamId}`)
        .then((response) => {
          const responseData = response.data;
          if (responseData["user_games"].length === 0) {
            this.error = `Pas de jeux trouvés avec l'id Steam <strong>${this.steamId}<br>Est-ce que votre profil est bien public ?<strong>`
          }
          this.processResponse(responseData);
        })
        .catch((error) => {
          if (error.response && error.response.status === 404) {
            this.error = error.response.data;
          }
        })
        .finally(() => {
          this.loadingForSteamId = false;
        });
    },

    processResponse(responseData) {
      this.step = "my_games";
      this.error = null;
      this.userInfos = responseData["user_infos"]
      this.foundGames = responseData["user_games"];
      this.steamId = this.userInfos.steam_id;

      localStorage.setItem("coincoinvapeur-games", JSON.stringify(responseData));
    },

    refreshProfileData() {
      if (!this.steamId) {
        this.steamId = this.userInfos.steam_id;
      }
      this.findMyGamesBySteamId();
    },

    cleanProfileData() {
      localStorage.removeItem("coincoinvapeur-games");
      this.steamNickname = ""
      this.userInfos = null;
      this.foundGames = []
      this.step = "login"
    },

    toogleCritic(game) {
      game.showCritic = !game.showCritic;
    },

    enableUIComponents() {
      $(".ui.checkbox").checkbox();
      $(".ui.dropdown").dropdown();
    },

    convertTime(minutes) {
      return luxon.Duration
        .fromObject({minutes: minutes})
        .shiftTo("hour", "minute")
        .toFormat("hh'h'mm'min'")
    },

    colorishScore(game) {
      if (game.score === -1) {
        if (game.otherScore === "Non testé") {
          return "pink";
        }
        return "purple"
      }
      return this.findColor(game.score);
    },

    displayScore(game) {
      if (game.score === -1) {
        return game.otherScore;
      }
      return game.score;
    },

    formatGameYear(year) {
      if (year > 0) {
        return year;
      }
      return "NC"
    },

    displayPrice(discountInfo) {
      if (discountInfo && discountInfo.discountPrice) {
        const formattedPrice = Number(discountInfo.discountPrice / 100)
          .toLocaleString(
            'fr-FR',
            {
              style: 'currency',
              currency: 'EUR'
            });
        const percent = discountInfo.discountPercent === 0
          ? ""
          : `(-${discountInfo.discountPercent} %)`
        return `${formattedPrice} ${percent}`;
      }
      return "NC";
    },

    findColor(score) {
      if (score > 9) {
        return "blue";
      } else if (score > 7) {
        return "green";
      } else if (score > 6) {
        return "olive";
      } else if (score > 5) {
        return "yellow";
      } else if (score > 4) {
        return "orange";
      } else {
        return "red";
      }
    },

    titleMatch(searchGame, regex, game) {
      if (this.searchGame.length < 3) {
        return true;
      }
      return regex.test(game.title);
    },

    neverPlayer(game) {
      return !this.displayNeverPlayedGameOnly || game.steamInfo.playtime_forever === 0;
    },

    fromWishList(game) {
      return !this.displayWishListOnly || game.steamInfo.wished;
    }
  }
}).mount("#canardVapeur");