const {createApp} = Vue;

const timeLabels = ["-30'", "1h", "1h30", "2h+"];
const priceLabels = ["-20 €", "30 €", "40 €", "50 €", "60 €+"];
const peopleLabels = ["1", "2", "3", "4", "5+"];
const complexityLabels = ["accessible", "intermédiaire", "complexe"];
const gameStylesEntries = [
    {
        value: 'ambiance',
        label: 'Ambiance'
    },
    {
        value: 'cartes',
        label: 'Cartes'
    },
    {
        value: 'cooperatif',
        label: 'Coopératif'
    },
    {
        value: 'extension',
        label: 'Extension'
    },
    {
        value: 'familial',
        label: 'Familial'
    },
    {
        value: 'gestion',
        label: 'Gestion'
    },
    {
        value: 'solo-duo',
        label: 'Solo / duo'
    },
    {
        value: 'strategie',
        label: 'Stratégie'
    }
];

const defaultFiterValues = {
    selectedGameStyles: [],
    complexity: [0, 1],
    duration: [30, 60],
    nbPlayers: [2, 4],
    price: [20, 40],
}
const maxFiterValues = {
    selectedGameStyles: [],
    complexity: [0, 2],
    duration: [30, 120],
    nbPlayers: [1, 5],
    price: [20, 60],
}

createApp({
    compilerOptions: {
        delimiters: ['[[', ']]'],
    },
    data() {
        return {
            selectedGameStyles: defaultFiterValues.selectedGameStyles,
            duration: defaultFiterValues.duration,
            nbPlayers: defaultFiterValues.nbPlayers,
            price: defaultFiterValues.price,
            complexity: defaultFiterValues.complexity,
        }
    },
    mounted() {
        const savedRawFilter = localStorage.getItem('coin-de-table-filter');
        if (savedRawFilter) {
            const savedFilter = JSON.parse(savedRawFilter);


            this.selectedGameStyles = savedFilter.gameStyles ? savedFilter.gameStyles : defaultFiterValues.selectedGameStyles;
            this.duration = savedFilter.duration ? savedFilter.duration : defaultFiterValues.duration;
            this.nbPlayers = savedFilter.nbPlayers ? savedFilter.nbPlayers : defaultFiterValues.nbPlayers;
            this.price = savedFilter.price ? savedFilter.price : defaultFiterValues.price;
            this.complexity = savedFilter.complexity ? savedFilter.complexity : defaultFiterValues.complexity;
        }

        $('#time-slider-range')
            .slider({
                interpretLabel: (value) => {
                    return timeLabels[value];
                },
                onChange: (value, min, max) => {
                    this.duration = [min, max];
                },
                min: maxFiterValues.duration[0],
                max: maxFiterValues.duration[1],
                step: 30,
                start: this.duration[0],
                end: this.duration[1],
            });

        $('#people-slider-range')
            .slider({
                interpretLabel: (value) => {
                    return peopleLabels[value];
                },
                onChange: (value, min, max) => {
                    this.nbPlayers = [min, max];
                },
                min: maxFiterValues.nbPlayers[0],
                max: maxFiterValues.nbPlayers[1],
                step: 1,
                start: this.nbPlayers[0],
                end: this.nbPlayers[1],
            });

        $('#price-slider-range')
            .slider({
                interpretLabel: (value) => {
                    return priceLabels[value];
                },
                onChange: (value, min, max) => {
                    this.price = [min, max];
                },
                min: maxFiterValues.price[0],
                max: maxFiterValues.price[1],
                step: 10,
                start: this.price[0],
                end: this.price[1]
            });

        $('#game-complexity-slider-range')
            .slider({
                interpretLabel: (value) => {
                    return complexityLabels[value];
                },
                onChange: (value, min, max) => {
                    this.complexity = [min, max];
                },
                min: maxFiterValues.complexity[0],
                max: maxFiterValues.complexity[1],
                step: 1,
                start: this.complexity[0],
                end: this.complexity[1],
            });

        const $gameStyle = $('#game-style');
        $gameStyle.dropdown({
            clearable: true,
            values: gameStylesEntries.map((gameStyle) => ({
                value: gameStyle.value,
                name: gameStyle.label,
                selected: this.selectedGameStyles.includes(gameStyle.value)
            })),
            onChange: (value) => {
                const gameStyles = value.split(',');
                this.selectedGameStyles = _.isEqual(gameStyles, [""]) ? defaultFiterValues.selectedGameStyles : gameStyles;
            }
        });

        //hack to make invisible selected game styles  whereas the flyout is hidden
        const visibleLabels = $gameStyle.find('a.ui.label.transition');
        visibleLabels.each(index => {
            $(visibleLabels[index]).removeClass(['transition', 'visible', 'animating', 'in', 'scale']);
        });
    },
    computed: {
        durationLabel() {
            const minDuration = this.duration[0];
            const maxDuration = this.duration[1];
            const lowestDuration = maxFiterValues.duration[0];
            const highestDuration = maxFiterValues.duration[1];

            if (minDuration === maxDuration) {
                if (minDuration === lowestDuration) {
                    return `${minDuration} minutes ou moins`;
                } else {
                    if (minDuration === highestDuration) {
                        return `${maxDuration} minutes ou plus`
                    }
                }
                return `${minDuration} minutes`;
            } else if (minDuration === lowestDuration && maxDuration === highestDuration) {
                return 'peu importe';
            } else if (minDuration === lowestDuration) {
                return `jusqu'à ${maxDuration} minutes`;
            } else if (maxDuration === highestDuration) {
                return `à partir de ${minDuration} minutes`;
            }
            return `entre ${minDuration} et ${maxDuration} minutes`;
        },
        complexityLabel() {
            const minComplexity = this.complexity[0];
            const maxComplexity = this.complexity[1];
            if (minComplexity === maxComplexity) {
                return `Complexité : ${complexityLabels[minComplexity]}`
            } else {
                if (minComplexity === maxFiterValues.complexity[0] && maxComplexity === maxFiterValues.complexity[1]) {
                    return 'peu importe';
                }
            }
            return `Complexité : de ${complexityLabels[minComplexity]} à ${complexityLabels[maxComplexity]}`;
        },
        nbPlayersLabel() {
            const minNbPlayers = this.nbPlayers[0];
            const maxNbPlayers = this.nbPlayers[1];

            const lowestNbPlayers = maxFiterValues.nbPlayers[0];
            const highestNbPlayers = maxFiterValues.nbPlayers[1];
            if (minNbPlayers === maxNbPlayers) {
                if (maxNbPlayers === highestNbPlayers) {
                    return `${minNbPlayers} ou plus`;
                }
                return `${minNbPlayers} personnes`
            } else {
                if (minNbPlayers === lowestNbPlayers && maxNbPlayers === highestNbPlayers) {
                    return 'peu importe';
                }
            }
            return `Entre ${minNbPlayers} et ${maxNbPlayers} personnes`;
        },
        priceLabel() {
            const minPrice = this.price[0];
            const maxPrice = this.price[1];

            const lowestPrice = maxFiterValues.price[0];
            const highestPrice = maxFiterValues.price[1];
            if (minPrice === maxPrice) {
                if (minPrice === lowestPrice) {
                    return `${minPrice} € ou moins`;
                } else if (minPrice === highestPrice) {
                    return `${minPrice} € ou plus`;
                }
                return `${minPrice} €`;
            } else if (minPrice === lowestPrice && maxPrice === highestPrice) {
                return 'peu importe';
            } else if (minPrice === lowestPrice) {
                return `jusqu'à ${maxPrice} €`
            } else if (maxPrice === highestPrice) {
                return `à partir de ${minPrice} €`;
            }
            return `Entre ${minPrice} et ${maxPrice} €`;
        }
    },
    methods: {
        resetFilter() {
            this.selectedGameStyles = defaultFiterValues.selectedGameStyles;
            this.duration = defaultFiterValues.duration;
            this.nbPlayers = defaultFiterValues.nbPlayers;
            this.price = defaultFiterValues.price;
            this.complexity = defaultFiterValues.complexity;

            $('#game-style').dropdown('clear', true);

            $('#time-slider-range').slider('set rangeValue', this.duration[0], this.duration[1], false);
            $('#price-slider-range').slider('set rangeValue', this.price[0], this.price[1], false);
            $('#people-slider-range').slider('set rangeValue', this.nbPlayers[0], this.nbPlayers[1], false);
            $('#game-complexity-slider-range').slider('set rangeValue', this.complexity[0], this.complexity[1], false);

            localStorage.removeItem('coin-de-table-filter');
        },
        searchWithFilter() {
            localStorage.setItem("coin-de-table-filter", JSON.stringify({
                gameStyles: this.selectedGameStyles,
                complexity: this.complexity,
                duration: this.duration,
                nbPlayers: this.nbPlayers,
                price: this.price,
            }));

            const filter = {};
            if (!_.isEqual(this.selectedGameStyles, defaultFiterValues.selectedGameStyles)) {
                filter.gameStyles = this.selectedGameStyles;
            }
            if (!_.isEqual(this.duration, maxFiterValues.duration)) {
                filter.duration = this.duration;
            }
            if (!_.isEqual(this.complexity, maxFiterValues.complexity)) {
                filter.complexity = this.complexity;
            }
            if (!_.isEqual(this.nbPlayers, maxFiterValues.nbPlayers)) {
                filter.nbPlayers = this.nbPlayers;
            }
            if (!_.isEqual(this.price, maxFiterValues.price)) {
                filter.price = this.price;
            }

            window.location = `${window.location.origin}/coin-de-table/search.html?filter=${JSON.stringify(filter, null, '')}`;
        }
    }
}).mount('#plato');
