# -*- coding: utf-8 -*-
import atexit
import datetime
import json
import os

from bson import json_util
from flask import render_template, request, jsonify, Blueprint
from flask_login import login_required, current_user

from data_access import MagazineDao, ReviewDao, ReviewerDao, EventDao, SteamAppInfoDao
from my_exceptions import InvalidData
from my_mongo_client import MyMongoClientWriter
from security_access import SecurityDaoReader, SecurityDaoWriter
from steam_client import SteamClient

bp = Blueprint('admin', __name__, template_folder='templates')

env = "test"
if 'ENV' in os.environ:
    env = os.environ['ENV']

mongo_client_writer = MyMongoClientWriter(env)


def close_mongo():
    mongo_client_writer.close()


atexit.register(close_mongo)


@bp.route('/canardeurs.html')
@login_required
def canardeurs():
    user_id = current_user.email
    events = event_dao(mongo_client_writer).get_last_events()
    magazines = magazine_dao(mongo_client_writer).get_empty_magazines()
    empty_critics_reviews = review_dao(mongo_client_writer).get_reviews_without_critic()
    return render_template('admin/v3/canardeurs.html', user_id=user_id, events=list(events), empty_magazines=magazines,
                           empty_critics_reviews=empty_critics_reviews)


@bp.route('/magazines.html')
@login_required
def admin_magazines_html():
    user_id = current_user.email
    return render_template('admin/v3/magazines.html', user_id=user_id)


@bp.route('/magazines')
@login_required
def admin_magazines():
    magazines = magazine_dao(mongo_client_writer).get_magazines()
    return json_util.dumps(magazines)


@bp.route('/magazine/save', methods=['POST'])
@login_required
def create_magazine():
    magazine_data = json.loads(request.data)
    oid = magazine_dao(mongo_client_writer).create_magazine(magazine_data)
    review_dao(mongo_client_writer).update_magazine_issue(magazine_data)

    event_dao(mongo_client_writer).save_event({
        'login': current_user.email,
        'collection': 'magazine',
        'timestamp': datetime.datetime.utcnow(),
        'id': magazine_data['issueNumber']
    })
    return jsonify(status="SUCCESS", id=str(id))


@bp.route('/magazine/save/<id>', methods=['PUT'])
@login_required
def update_magazine(id):
    magazine_data = json.loads(request.data)
    magazine_dao(mongo_client_writer).update_magazine(id, magazine_data)
    review_dao(mongo_client_writer).update_magazine_issue(magazine_data)

    event_dao(mongo_client_writer).save_event({
        'login': current_user.email,
        'collection': 'magazine',
        'timestamp': datetime.datetime.utcnow(),
        'id': magazine_data['issueNumber']
    })
    return jsonify(status="SUCCESS", id=id)


@bp.route('/reviewers.html')
@login_required
def admin_reviewers_html():
    user_id = current_user.email
    return render_template('admin/v3/reviewers.html', user_id=user_id)


@bp.route('/reviewer/save', methods=['POST'])
@login_required
def create_reviewer():
    reviewer_data = json.loads(request.data)
    id = reviewer_dao(mongo_client_writer).create_reviewer(reviewer_data)
    review_dao(mongo_client_writer).update_reviewer(reviewer_data)
    event_dao(mongo_client_writer).save_event({
        'login': current_user.email,
        'collection': 'testeur',
        'timestamp': datetime.datetime.utcnow(),
        'id': reviewer_data['name']
    })
    return jsonify(status="SUCCESS", id=str(id))


@bp.route('/reviewer/save/<id>', methods=['PUT'])
@login_required
def update_reviewer(id):
    reviewer_data = json.loads(request.data)
    reviewer_dao(mongo_client_writer).update_reviewer(id, reviewer_data)
    review_dao(mongo_client_writer).update_reviewer(reviewer_data)
    event_dao(mongo_client_writer).save_event({
        'login': current_user.email,
        'collection': 'testeur',
        'timestamp': datetime.datetime.utcnow(),
        'id': reviewer_data['name']
    })
    return jsonify(status="SUCCESS", id=id)


@bp.route('/reviews.html')
@login_required
def admin_reviews_html():
    user_id = current_user.email
    return render_template('admin/v3/reviews.html', user_id=user_id)


@bp.route('/review/save', methods=['POST'])
@login_required
def create_review():
    review_data = json.loads(request.data)
    try:
        if 'steamAppId' in review_data:
            steam_appid = review_data['steamAppId']
            steam_game_info = fetch_and_save_steam_game_info_if_needed(steam_appid)

            if steam_game_info:
                review_data['coverTnUrl'] = steam_game_info['header_image']

        id = review_dao(mongo_client_writer).create_review(review_data)

        event_dao(mongo_client_writer).save_event({
            'login': current_user.email,
            'collection': 'test',
            'timestamp': datetime.datetime.utcnow(),
            'id': review_data['title']
        })
        return jsonify(status="SUCCESS", id=str(id))
    except InvalidData as ex:
        return jsonify(status="FAIL", errors=ex.errors)


@bp.route('/review/save/<id>', methods=['PUT'])
@login_required
def update_review(id):
    json_data = json.loads(request.data)
    try:
        if 'steamAppId' in json_data:
            steam_appid = json_data['steamAppId']
            steam_game_info = fetch_and_save_steam_game_info_if_needed(steam_appid)

            if steam_game_info:
                json_data['coverTnUrl'] = steam_game_info['header_image']

        review_dao(mongo_client_writer).update_review(id, json_data)

        event_dao(mongo_client_writer).save_event({
            'login': current_user.email,
            'collection': 'test',
            'timestamp': datetime.datetime.utcnow(),
            'id': json_data['title']
        })
        return jsonify(status="SUCCESS", id=id)
    except InvalidData as ex:
        return jsonify(status="FAIL", errors=ex.errors)


def fetch_and_save_steam_game_info_if_needed(steam_appid):
    if steam_appid:
        steam_app_info_dao = steam_app_dao(mongo_client_writer)
        game_info = steam_app_info_dao.get_game_info_by_id(steam_app_id=steam_appid)

        steam_game_info = steam_client().fetch_game_info(steam_appid=steam_appid)
        if steam_game_info:
            if game_info:
                steam_game_info['_id'] = game_info['_id']
                steam_app_info_dao.update_game_info(game_info['_id'], steam_game_info)
            else:
                steam_app_info_dao.create_game_info(steam_game_info)
            return steam_game_info
    return None


def steam_client():
    return SteamClient(api_key=os.environ['STEAM_API_KEY'])


@bp.route('/reviews', methods=['GET'])
@login_required
def admin_reviews():
    reviews = review_dao(mongo_client_writer).get_all_reviews()
    return json_util.dumps(reviews)


@bp.route('/users.html')
@login_required
def users_html():
    user_id = current_user.email
    return render_template('admin/users.html', user_id=user_id)


@bp.route('/users')
@login_required
def tools():
    users = SecurityDaoReader(mongo_client_writer).get_users()
    return json_util.dumps(users)


@bp.route('/user/save', methods=['POST'])
@login_required
def save_user():
    json_data = json.loads(request.data)
    oid = SecurityDaoWriter(mongo_client_writer).save_user(json_data)
    return jsonify(status="SUCCESS", oid=str(oid))


def review_dao(mongo_client):
    return ReviewDao(mongo_client)


def reviewer_dao(mongo_client):
    return ReviewerDao(mongo_client)


def magazine_dao(mongo_client):
    return MagazineDao(mongo_client)


def steam_app_dao(mongo_client):
    return SteamAppInfoDao(mongo_client)


def event_dao(mongo_client):
    return EventDao(mongo_client)
