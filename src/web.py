# -*- coding: utf-8 -*-

import atexit
import os
import sys

from flask import render_template, Flask, request, redirect, send_from_directory
from flask_login import LoginManager, login_user, logout_user, login_required, current_user
from flask_mongoengine import MongoEngine
from flask_security import LoginForm, RoleMixin, UserMixin, MongoEngineUserDatastore, Security
from mongoengine import Document, StringField, ListField, ReferenceField, BooleanField, DateTimeField

import admin
import api
import plato
import vapor
from data_access import MagazineDao, ReviewDao, ReviewerDao, SteamAppInfoDao, BoardReviewDao
from my_exceptions import MissingMongoConfiguration
from my_mongo_client import MyMongoClientReader
from security_access import hash_data, SecurityDaoReader


BOARD_GAME_CATEGORY = {
    'familial' : 'Familial',
    'cartes': 'Cartes',
    'solo-duo': 'Solo / duo',
    'ambiance': 'Ambiance',
    'strategie': 'Stratégie',
    'gestion': 'Gestion',
    'cooperatif': 'Coopératif'
}

env = "test"
if 'ENV' in os.environ:
    env = os.environ['ENV']
mongo_client_reader = MyMongoClientReader(env)

login_manager = LoginManager()


def create_app():
    main_app = Flask(__name__)

    if 'SECRET_KEY' in os.environ:
        main_app.config['SECRET_KEY'] = os.environ['SECRET_KEY']

    main_app.config['SECURITY_REGISTERABLE'] = False

    if 'DEBUG' in os.environ:
        main_app.config['DEBUG'] = True
        main_app.config['TEMPLATES_AUTO_RELOAD'] = True

    main_app.config['MONGO_READER'] = mongo_client_reader

    login_manager.init_app(main_app)

    main_app.register_blueprint(admin.bp, url_prefix='/admin')
    main_app.register_blueprint(vapor.bp, url_prefix='/vapor')
    main_app.register_blueprint(api.bp, url_prefix='/api')
    main_app.register_blueprint(plato.bp, url_prefix='/coin-de-table')

    return main_app


app = create_app()

# MongoDB Config
app.config['MONGODB_SETTINGS'] = {
    'host': os.environ["MONGO_SETTINGS"],
    'connect': False
}

# Create database connection object
mongo_engine = MongoEngine(app)


class Role(Document, RoleMixin):
    name = StringField(max_length=80, unique=True)
    description = StringField(max_length=255)


class User(Document, UserMixin):
    email = StringField(max_length=255)
    password = StringField(max_length=255)
    active = BooleanField(default=True)
    confirmed_at = DateTimeField()
    roles = ListField(ReferenceField(Role), default=[])


# Setup Flask-Security
user_datastore = MongoEngineUserDatastore(mongo_engine, User, Role)
security = Security()
security.init_app(app, user_datastore)


def close_mongo():
    mongo_client_reader.close()


atexit.register(close_mongo)


def format_datetime_fr(date):
    return u'le {day} à {hour}'.format(day=date.strftime('%d/%m/%Y'), hour=date.strftime('%H:%M:%S'))

def format_game_board_category(category):
    return BOARD_GAME_CATEGORY[category]

def dash(a_string):
    return a_string.replace(' ', '-')


app.jinja_env.filters['datetime_fr'] = format_datetime_fr
app.jinja_env.filters['board_game_label'] = format_game_board_category
app.jinja_env.filters['dash'] = dash


@login_manager.user_loader
def load_user(userid):
    return User.objects(email=userid).first()


def _build_api_token(host_url):
    return hash_data(host_url)


@app.route('/')
@app.route('/index.html')
def index():
    local_review_dao = review_dao(mongo_client_reader)
    statistics = dict(videoGameEntryCount=local_review_dao.get_count(),
                      issueCount=magazine_dao(mongo_client_reader).get_count(),
                      steamInfoCount=local_review_dao.get_count_with_steam())
    return render_template('v3/index.html',
                           user_id=get_user_id(),
                           statistics=statistics,
                           api_token=_build_api_token(request.host_url))

@app.route('/robots.txt')
@app.route('/sitemap.xml')
def static_from_root():
    return send_from_directory(app.static_folder, request.path[1:])

@app.route("/signin", methods=["GET", "POST"])
def signin():
    form = LoginForm()
    if request.method == 'POST':
        email = form.email.data
        password = form.password.data
        user = User.objects(email=email).first()

        if user and hash_data(password) == user.password:
            login_user(user, remember=True)
            return redirect("/admin/canardeurs.html")

    return render_template('admin/v3/login.html', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect('/')


@app.route('/canard-vapeur.html')
def canard_vapeur():
    return render_template('v3/vapor/canard-vapeur.html')


@app.route('/search.html', methods=['GET'])
def search():
    search_value = request.args.get('searchInput', '')
    if search_value:
        reviews = review_dao(mongo_client_reader).get_all_review_by_title(search_value)
        return render_template('v3/searchResults.html',
                               reviews=reviews,
                               search_value=search_value,
                               user_id=get_user_id(),
                               api_token=_build_api_token(request.host_url))


@app.route('/magazines.html')
def magazines_html():
    magazine_years = magazine_dao(mongo_client_reader).get_all_magazine_years()

    year = request.args.get('year', None)
    if year is None:
        year = magazine_years[-1]

    magazines = magazine_dao(mongo_client_reader).get_all_magazines_by_year(year)
    return render_template('v3/magazines.html', magazine_years=magazine_years, magazines=magazines, year=int(year),
                           user_id=get_user_id())


def get_user_id():
    return current_user.email if hasattr(current_user, 'email') else None


@app.route('/magazine/<issue_number>.html')
def magazine(issue_number):
    found_magazine = magazine_dao(mongo_client_reader).get_magazine_by_issue_number(int(issue_number))

    board_reviews = board_review_dao(mongo_client_reader).get_all_reviews_by_mag_issue_number(str(issue_number))
    if board_reviews:
        found_magazine['reviews']['board'] = board_reviews

    return render_template('v3/magazine.html', magazine=found_magazine)


@app.route('/review/<review_id>.html', methods=['GET'])
def review_by_id(review_id):
    review = review_dao(mongo_client_reader).get_review_by_oid(review_id)
    if 'steamAppId' in review and review['steamAppId']:
        steam_game_info = steam_app_dao(mongo_client_reader).get_game_info_by_id(review['steamAppId'])
        review['game_info'] = steam_game_info
    return render_template('v3/review.html', review=review)


@app.route('/indexes.html')
def indexes():
    return render_template('v3/indexes.html')


@app.route('/indexes/title.html')
def index_title():
    reviews = review_dao(mongo_client_reader).get_all_review_titles()
    return render_template('v3/index_title.html', reviews=reviews)


@app.route('/indexes/genre.html')
def index_genre():
    reviews = review_dao(mongo_client_reader).get_all_genres()
    return render_template('v3/index_genre.html', reviews=reviews)


@app.route('/indexes/score.html')
def index_score():
    reviews = review_dao(mongo_client_reader).get_all_scores()
    return render_template('v3/index_score.html', reviews=reviews)


@app.route('/indexes/reviewer.html')
def index_reviewer():
    reviews = review_dao(mongo_client_reader).get_all_reviewers()
    return render_template('v3/index_reviewer.html', reviews=reviews)


@app.route('/about.html')
def about():
    return render_template('v3/about.html')


@app.route('/studio/<studio_name>.html')
def studio(studio_name):
    reviews = review_dao(mongo_client_reader).get_reviews_by_studio(studio_name)
    return render_template('v3/studio.html', studio_name=studio_name, reviews=reviews)


@app.route('/genre/<genre_name>.html')
def genre(genre_name):
    reviews = review_dao(mongo_client_reader).get_reviews_by_genre(genre_name)
    return render_template('v3/genre.html', genre_name=genre_name, reviews=reviews)


@app.route('/reviewer/<reviewer_name>.html')
def reviewer(reviewer_name):
    reviews = review_dao(mongo_client_reader).get_reviews_by_reviewer(reviewer_name)
    return render_template('v3/reviewer.html', reviewer_name=reviewer_name, reviews=reviews)


@app.route('/publisher/<publisher_name>.html')
def publisher(publisher_name):
    reviews = review_dao(mongo_client_reader).get_reviews_by_publisher(publisher_name)
    return render_template('v3/publisher.html', publisher_name=publisher_name, reviews=reviews)


@app.route('/google2cbe3d01e021f3ed.html')
def google():
    return render_template('google2cbe3d01e021f3ed.html')


@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_error(error):
    return render_template('500.html'), 500


def review_dao(mongo_client):
    return ReviewDao(mongo_client)


def board_review_dao(mongo_client):
    return BoardReviewDao(mongo_client)


def reviewer_dao(mongo_client):
    return ReviewerDao(mongo_client)


def magazine_dao(mongo_client):
    return MagazineDao(mongo_client)


def steam_app_dao(mongo_client):
    return SteamAppInfoDao(mongo_client)


def security_dao(mongo_client):
    return SecurityDaoReader(mongo_client)


def display_usage():
    print("Usage: python web.py <env=[test|prod]> <debug=[True|False]>")


if __name__ == '__main__':
    try:
        app.run(port=4000)
    except MissingMongoConfiguration as ex:
        print(ex)
        mongo_client_reader.close()
        sys.exit(-1)
