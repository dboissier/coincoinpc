# -*- coding: utf-8 -*-

from bson import json_util
from flask import Blueprint, current_app
from flask import request

from data_access import ReviewerDao, ReviewDao, BoardReviewDao
from security_access import hash_data

bp = Blueprint('api', __name__, template_folder='templates')


def _prepare_reviews(reviews):
    return [{
                'title': review['title'],
                'review_url': "/review/{_id}.html".format(_id=review['_id'])
            }
            for review in reviews]

def _prepare_board_reviews(reviews):
    return [{
                'title': review['title'],
                'review_url': "/coin-de-table/review/{_id}.html".format(_id=review['_id'])
            }
            for review in reviews]


def _check_api_token(host_url, token):
    if not token or hash_data(host_url) != token:
        raise Exception("Invalid token")


@bp.route('/search')
def search_review():
    _check_api_token(request.host_url, request.args.get('apiToken'))
    search_value = request.args.get('searchInput', '')
    if search_value:
        reviews = review_dao(get_mongo_client_reader()).get_all_review_by_title(search_value)
        return json_util.dumps({'reviews': _prepare_reviews(reviews)})
    return '{}'


@bp.route('/plato/search')
def search_board_review():
    _check_api_token(request.host_url, request.args.get('apiToken'))
    search_value = request.args.get('searchInput', '')
    if search_value:
        reviews = board_review_dao(get_mongo_client_reader()).get_all_review_by_title(search_value)
        return json_util.dumps({'reviews': _prepare_board_reviews(reviews)})
    return '{}'


@bp.route('/reviewers')
def reviewers():
    all_reviewers = reviewer_dao(get_mongo_client_reader()).get_all_reviewers()
    return json_util.dumps(all_reviewers)


@bp.route('/genres')
def genre():
    genres = review_dao(get_mongo_client_reader()).get_all_genres_name()
    return json_util.dumps(genres)


def review_dao(mongo_client):
    return ReviewDao(mongo_client)

def board_review_dao(mongo_client):
    return BoardReviewDao(mongo_client)


def reviewer_dao(mongo_client):
    return ReviewerDao(mongo_client)


def get_mongo_client_reader():
    return current_app.config['MONGO_READER']
