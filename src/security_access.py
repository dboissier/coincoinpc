import os

from hashlib import sha256
from bson import ObjectId
from flask_login import UserMixin
from itsdangerous import URLSafeSerializer

secret_key = os.environ['SECRET_KEY']

login_serializer = URLSafeSerializer(secret_key)


def hash_data(data_to_hash):
    """
    Return the md5 hash of the data+salt
    """
    salted_data = data_to_hash + secret_key
    return sha256(salted_data.encode('utf-8')).hexdigest()


class SecurityDaoReader():
    def __init__(self, mongo_client):
        self.database = mongo_client.database

    def get_user(self, userid):
        found_user = self.database['user'].find_one({'username': userid})

        if found_user:
            return User(found_user['username'], found_user['password'], found_user['role'])

    def get_users(self):
        return self.database['user'].find({}, {'username': 1, 'role': 1})


class SecurityDaoWriter():
    def __init__(self, mongo_client):
        self.database = mongo_client.database

    def save_user(self, user_to_save):
        if user_to_save['password'] != '':
            user_to_save['password'] = hash_data(user_to_save['password'])

        if user_to_save['_id'] == '':
            user_to_save.pop('_id', None)
        else:
            user_to_save['_id'] = ObjectId(user_to_save['_id'])

        self.database['user'].save(user_to_save)

    def delete_user(self, user_id):
        self.database['user'].delete({'_id': ObjectId(user_id)})


class User(UserMixin):
    def __init__(self, userid, password, role):
        self.id = userid
        self.password = password
        self.role = role

    def get_auth_token(self):
        """
        Encode a secure token for cookie
        """
        data = [str(self.id), self.password, self.role]
        return login_serializer.dumps(data)
