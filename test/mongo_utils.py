import json


class MongoUtils:
    def __init__(self, mongo_client):
        self.database = mongo_client.database

    def insertJsonFile(self, collectionName, filename, index_column, delete_before_insert=True, parent_folder='data'):
        with open("%s/%s" % (parent_folder, filename), 'rb') as file:
            data = file.read()
            jsonData = json.loads(data)
            self.insertData(collectionName, jsonData, index_column, delete_before_insert)

    def insertData(self, collectionName, jsonDataList, index_column, delete_before_insert):
        collection = self.database[collectionName]
        if delete_before_insert:
            collection.remove()
        for jsonData in jsonDataList:
            collection.insert(jsonData)
        collection.create_index(index_column)

    def clear_collection(self, collection_name):
        self.database[collection_name].remove({})
