import csv
import json
import time

import bson
import requests
from bson.objectid import ObjectId
from slugify import slugify

import data_utils
import mongo_utils
from my_mongo_client import MyMongoClientReader, MyMongoClientWriter
from steam_client import SteamClient

mongo_util = mongo_utils.MongoUtils(MyMongoClientReader(env='test'))


def generate_reviews_json_data(magazine_filename, review_filename):
    magazines = {}
    with open("prod/%s.json" % magazine_filename) as file_magazine:

        data_magazine = file_magazine.read()
        json_data_magazine = json.loads(data_magazine)
        for item in json_data_magazine:
            magazines[item['issueNumber']] = item['issueDate']

    reviewer = {}
    with open("prod/reviewer.json") as file_reviewer:
        data_review = file_reviewer.read()
        json_data_review = json.loads(data_review)
        for item in json_data_review:
            reviewer[item['name']] = item['reviewerTnUrl']

    with open("prod/%s.csv" % review_filename, 'rb') as file_input:

        reader = csv.DictReader(file_input, delimiter=';', fieldnames=[
            'title', 'subTitle', 'year', 'issue', 'reviewer', 'score', 'displayedGenre', 'primaryGenre',
            'secondaryGenre', 'studio', 'publisher', 'coverTnUrl', 'plateform', 'critic'
        ])

        all_row = []

        studio_names = []

        for row in reader:
            row['year'] = int(row['year'])

            if row['primaryGenre'] == '':
                row['primaryGenre'] = row['displayedGenre']

            if row['score'].isdigit():
                row['score'] = int(row['score'])
            else:
                row['otherScore'] = row['score']
                row['score'] = -1

            studio_and_country = data_utils.extract_studio_and_country(row['studio'])
            if studio_and_country:
                row['studioName'] = studio_and_country[0]
                row['country'] = studio_and_country[1]
            else:
                row['studioName'] = row['studio']

            if row['studioName'] not in studio_names:
                studio_names.append(row['studioName'])

            if row['publisher'] == '':
                row['publisher'] = row['studioName']

            row['year'] = int(row['year'])
            row['issue'] = {'issueNumber': int(row['issue']),
                            'coverTnUrl': 'http://www.canardpc.com/img/couv/couv_Canard_PC_%s_169.jpg' % row['issue'],
                            'issueDate': magazines[int(row['issue'])]}

            row['reviewer'] = {'name': row['reviewer'], 'reviewerTnUrl': reviewer[row['reviewer']]}
            if row['coverTnUrl'] is None or row['coverTnUrl'] == '':
                row['coverTnUrl'] = "../static/images/pas_de_jaquette.png"
            all_row.append(row)

        with open("prod/%s.json" % review_filename, 'wb') as review_json_file_output:
            review_json_file_output.write(json.dumps(all_row))

        with open("prod/studio.json", 'wb') as studio_json_file_output:
            studios = [{'name': elem, 'studioTnUrl': '../static/images/no-image_106px.png'} for elem in studio_names]
            studio_json_file_output.write(json.dumps(studios))


def generate_magazine_json_data(magazine_filename):
    with open("prod/%s.csv" % magazine_filename, 'rb') as file_input:
        reader = csv.DictReader(file_input, delimiter=';', fieldnames=['issueNumber', 'issueDate', 'title', 'subTitle'])

        all_row = []
        for row in reader:
            row['issueNumber'] = int(row['issueNumber'])
            row['coverUrl'] = "http://www.canardpc.com/img/couv/couv_Canard_PC_%s.jpg" % row['issueNumber']
            row['coverTnUrl'] = "http://www.canardpc.com/img/couv/couv_Canard_PC_%s_169.jpg" % row['issueNumber']
            all_row.append(row)

        with open("prod/%s.json" % magazine_filename, 'wb') as file_output:
            file_output.write(json.dumps(all_row))


def generate_genre_json_data():
    with open('prod/genre.csv', 'rb') as genre_file_input:
        reader = csv.DictReader(genre_file_input, delimiter=';', fieldnames=['genre'])
        all_genres = []
        for row in reader:
            all_genres.append(row['genre'])

        print(sorted(list(set(all_genres))))


def extract_studios_from_review_collection():
    with open("review_url.json", 'wb') as file_output:
        studios = [{'name': studio_name, "studioTnUrl": "../static/images/no-image_106px.png"}
                   for studio_name in mongo_util.database['review'].distinct('studioName')]

        file_output.write(json.dumps(studios))


def export_review_for_sebum():
    with open('data/export_for_steam_info.json', 'r') as export_file:
        json_data = json.load(export_file)
        with open('export_sebum.csv', 'w', newline='') as csvfile:
            fieldnames = ['titre', 'numero', 'testeur', 'note', 'critique']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for data in json_data:
                writer.writerow({
                    'titre': data['title'],
                    'numero': data['issue']['issueNumber'],
                    'testeur': data['reviewer']['name'],
                    'note': data['score'],
                    'critique': data['critic'],
                })


def update_review_with_steam_appid():
    with open('../review_with_steamid_1.csv', 'r') as reviews_with_steam_appid_file:
        csv_dict_reader = csv.DictReader(reviews_with_steam_appid_file, delimiter=',',
                                         fieldnames=['review_id', 'game_title', 'steam_appid'])
        mongo_writer = MyMongoClientWriter(env='prod')
        steam_client = SteamClient()
        try:
            review_collection = mongo_writer.database['review']
            steaminfo_collection = mongo_writer.database['steam_game_info']
            for row in csv_dict_reader:
                steam_appid = row['steam_appid']
                review_id = row['review_id']

                if not steam_appid:
                    print(f'No steamid for {review_id}/{row["game_title"]}')
                    continue

                print(f'Processing {review_id}/{row["game_title"]}, {steam_appid}')

                gameinfo_from_steam = steam_client.fetch_game_info(steam_appid=int(steam_appid))
                if not gameinfo_from_steam:
                    print(f'Unable to find steam info of {review_id}/{row["game_title"]} with id={steam_appid}')
                    continue

                gameinfo_from_db = steaminfo_collection.find_one({'steam_appid': int(steam_appid)})
                if gameinfo_from_db:
                    gameinfo_from_steam['_id'] = gameinfo_from_db['_id']
                steaminfo_collection.save(gameinfo_from_steam)

                review = review_collection.find_one({'_id': ObjectId(review_id)})
                review['steamAppId'] = int(steam_appid)
                review['coverTnUrl'] = gameinfo_from_steam['header_image']
                review_collection.save(review)

                time.sleep(2)
        except Exception as err:
            print(f'{err}')
        finally:
            mongo_writer.close()


def update_magazine_cover_url():
    mongo_writer = MyMongoClientWriter(env='prod')
    try:
        r = range(358, 419)
        l = list(r) + [345, 351]
        for issue_number in l:
            print(f'processing {issue_number}')
            magazine_collection = mongo_writer.database['magazine']
            magazine = magazine_collection.find_one({'issueNumber': issue_number})
            cover_url = f'https://assets.coincoinpc.fr/mag-covers/{issue_number}.png'
            magazine['coverTnUrl'] = cover_url
            magazine_collection.replace_one({'_id': magazine['_id']}, magazine)

            mongo_writer.database['review'].update_many({'issue.issueNumber': issue_number},
                                                        {'$set': {
                                                            'issue.coverTnUrl': cover_url
                                                        }})
    finally:
        mongo_writer.close()


def update_review_with_magazine():
    mongo_writer = MyMongoClientWriter(env='prod')
    try:
        review_collection = mongo_writer.database['review']
        magazine_collection = mongo_writer.database['magazine']

        for magazine in magazine_collection.find({'issueNumber': {'$gt': 160}},
                                                 {'_id': False,
                                                  'coverTnUrl': True,
                                                  'issueNumber': True,
                                                  'tableOfContentUrl': True}):
            for review in review_collection.find({'issue.issueNumber': magazine['issueNumber']}):
                review['issue'] = magazine
                review_collection.save(review)
            print('magazine %s updated' % magazine['issueNumber'])
    finally:
        mongo_writer.close()


def update_review_url():
    mongo_writer = MyMongoClientWriter(env='test')
    try:
        review_collection = mongo_writer.database['review']

        with open('test/review_new_urls.csv', newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',')
            for row in spamreader:
                object_id = row[0]
                review_url = row[1]
                if 'moteur-de-recherche' not in review_url:
                    print(f'updating object_id={object_id} with review_url={review_url}')

                result = review_collection.update_one({"_id": bson.ObjectId(object_id)},
                                                      {"$set": {"reviewUrl": review_url}})
                print(result.modified_count)
        print("Done")
    finally:
        mongo_writer.close()


def manage_external_review_cover():
    mongo_writer = MyMongoClientWriter(env='prod')
    try:
        review_collection = mongo_writer.database['review']

        with open('../review_cover_url.csv', newline='') as file:
            content = csv.DictReader(file)
            for row in content:
                image_content = requests.get(row['coverTnUrl'])
                title = slugify(row['title'])
                with open(f'/Users/davidboissier/dev/coincoinpc/s3/video-game-covers/{title}.jpg', 'wb') as image_file:
                    image_file.write(image_content.content)

                review_collection.update_one({"_id": bson.ObjectId(row['_id'])},
                                             {"$set": {
                                                 "coverTnUrl": f'https://assets.coincoinpc.fr/video-game-covers/{title}.jpg'
                                             }})
                time.sleep(1)

    finally:
        mongo_writer.close()


def import_board_magazine(filename: str, with_truncate: bool, env: str = None):
    mongo_writer = MyMongoClientWriter(env)

    try:
        magazine_collection = mongo_writer.database['board_magazine']

        with open(filename) as board_magazine_file:
            board_magazines = json.load(board_magazine_file)

            if with_truncate:
                magazine_collection.delete_many({})

            for board_magazine in board_magazines:
                magazine_collection.insert_one(board_magazine)
    finally:
        mongo_writer.close()


def import_board_reviews(filename: str, with_truncate: bool, env: str):
    mongo_writer = MyMongoClientWriter(env)

    try:
        review_collection = mongo_writer.database['board_review']

        if with_truncate:
            review_collection.delete_many({})

        with open(filename) as board_review_file:
            board_reviews = json.load(board_review_file)

            for board_review in board_reviews:
                review_collection.insert_one(board_review)
    finally:
        mongo_writer.close()


if __name__ == '__main__':
    env = 'test'
    import_board_magazine('./data/board_magazines.json', True, env)
    import_board_reviews('./data/37_board_reviews.json', True, env)
    import_board_reviews('./data/38_board_reviews.json', False, env)
    import_board_reviews('./data/39_board_reviews.json', False, env)
    import_board_reviews('./data/40_board_reviews.json', False, env)
    import_board_reviews('./data/41_board_reviews.json', False, env)
