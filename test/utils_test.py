from datadiff.tools import assert_equal
from nose.tools import assert_is_none

import data_utils


class TestDataUtils(object):
    def test_extract_studio_and_country(self):
        assert_equal(('Ubisoft', 'France'), data_utils.extract_studio_and_country('Ubisoft (France)'))
        assert_is_none(data_utils.extract_studio_and_country('Ubisoft'))

    def test_extract_year_of_issue(self):
        assert_equal(2006, data_utils.extract_year_of_issue({'issueDate': '15/02/2006'}))

    def test_group_by_alphabet(self):
        data = [{'pouet': 'Zombi U'}, {'pouet': 'cossacks'}, {'pouet': 'call of duty'}, {'pouet': 'anno 1410'},
                {'pouet': 'Alpha protocol'}]

        assert_equal([('A', [{'pouet': 'Alpha protocol'}, {'pouet': 'anno 1410'}]),
                      ('C', [{'pouet': 'call of duty'}, {'pouet': 'cossacks'}]),
                      ('Z', [{'pouet': 'Zombi U'}])], data_utils.group_by_alphabet(data, key='pouet'))

    def test_group_by_year(self):
        data = [
            {'issueNumber': 5, 'issueDate': '26/11/2003'},
            {'issueNumber': 1, 'issueDate': '26/04/2003'},
            {'issueNumber': 15, 'issueDate': '26/10/2004'},
            {'issueNumber': 20, 'issueDate': '26/11/2005'},
            {'issueNumber': 50, 'issueDate': '26/11/2006'},
            {'issueNumber': 45, 'issueDate': '26/05/2006'},
        ]
        self.maxDiff = None
        assert_equal([(2003, [{'issueNumber': 5, 'issueDate': '26/11/2003'},
                              {'issueNumber': 1, 'issueDate': '26/04/2003'}]),
                      (2004, [{'issueNumber': 15, 'issueDate': '26/10/2004'}]),
                      (2005, [{'issueNumber': 20, 'issueDate': '26/11/2005'}]),
                      (2006, [{'issueNumber': 50, 'issueDate': '26/11/2006'},
                              {'issueNumber': 45, 'issueDate': '26/05/2006'}])], data_utils.group_by_year(data))

    def test_group_by(self):
        assert_equal({3: ['foo', 'bar'],
                      4: ['toto']},
                     data_utils.group_by(len, ['foo', 'bar', 'toto']))

    def test_convert_list_of_pair_to_list_of_dict(self):
        list_of_pair = [(0, 10), (5, 5)]
        assert_equal([{'0': 10}, {'5': 5}],
                     data_utils.convert_list_of_pair_to_list_of_dict(list_of_pair))
