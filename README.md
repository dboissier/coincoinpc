CoincoinPC
=============

This webapp manages video game reviews from [CanardPC](http://www.canardpc.com), french magazine.

## Frameworks

* Language : python
* webapp : Flask
* Database : MongoDB
* Javascript : Vuejs, axios, lodash, luxon, JQuery (for Fomantic-Ui)
* CSS : Fomantic-UI

## Installation

* Setup a VirtualEnv : `bin/virtualenv ENV`
* Activate it : `ENV/bin/activate`
* Install all required packages : `pip install -r src/requirements.txt`


## Deployment

Some env var should be set
```
SECRET_KEY (salt key used for password md5 hashing)
ENV (prod|test)
MONGO_HOST
MONGO_PORT
MONGO_DATABASE
MONGO_USERNAME (read only user)
MONGO_PASSWORD 
MONGO_USERNAME_RW (read write user)
MONGO_PASSWORD_RW
STEAM_KEY
```

* Production: deployment on clever-cloud platform, DNS: https://coincoinpc.fr
* Local: `python web.py`
